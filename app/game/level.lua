---@diagnostic disable: need-check-nil
local fiber = require('fiber')
local log = require("log")
local json = require('json')
local utils = require('app.tools.utils')
local errors = require('errors')
local uuid = require('uuid')
local t2s = require('app.tools.t2s')
local base = require('app.game.base')
local config = require('app.game.config')

local TRY_COUNT = 100

local function get_transfer_index_set_from_offer_index(transfer_set, offer_index)
    -- return set of transfer_index with offer in path: {transfer_index_set}
    local return_set = {}
    for i, t in ipairs(transfer_set) do
        for _, o in ipairs(t.path) do
            if offer_index == o then
                table.insert(return_set, i)
                break
            end
        end
    end
    return return_set
end

local function get_offer_index_set_started_from_card(offer_set, card)
    -- return set of offer_index with card in orig_set: {offer_index_set}
    local return_set = {}
    for i, s in ipairs(offer_set) do
        for _, o in ipairs(s.orig_set) do
            if card == o then
                table.insert(return_set, i)
                break
            end
        end
    end
    return return_set
end

local function get_all_beginning_from_card(transfer_set, offer_set, card)
    -- return set of transfer_index: {transfer_index_set}, which intersect with cards stay before card arg
    local return_set = {}
    local started_offer_index_set = {}
    local transfer_index_set = {}
    for oi, o in ipairs(offer_set) do
        for _, flip in ipairs(o.flip_set) do
            if flip == card then 
                for _, orig in ipairs(o.orig_set) do
                    started_offer_index_set = get_offer_index_set_started_from_card(offer_set, orig)
                    for _, s in ipairs(started_offer_index_set) do
                        transfer_index_set = utils.complement_table(
                            transfer_index_set, 
                            get_transfer_index_set_from_offer_index(transfer_set, s)
                        )
                    end
                    return_set = get_all_beginning_from_card(transfer_set, offer_set, orig)
                    transfer_index_set = utils.complement_table(transfer_index_set, return_set)                    
                end
            end
        end
    end
    return transfer_index_set
end

local function get_path_set_beginning_from_card(offer_set, card)
    -- return path_set: {{own, path}}, where path - beginning of path from own to card

    local return_set = {}
    local offer_return_set = {}
    local ret = {}
    local offer_found = false
    local worked_offer_found  = false
    local all_previous_offer_orig_reachable
    for oi, o in ipairs(offer_set) do
        for _, flip in ipairs(o.flip_set) do
            if flip == card then 
                offer_found = true
                if o.cancel ~= true then
                    worked_offer_found = true
                    all_previous_offer_orig_reachable = true
                    for _, orig in ipairs(o.orig_set) do
                        ret = get_path_set_beginning_from_card(offer_set, orig)
                        if #ret == 0 then
                            all_previous_offer_orig_reachable = false
                            break -- next offer
                        end
                        for _, r in ipairs(ret) do
                            table.insert(r.path, oi)
                        end                    
                        utils.add_full_table(offer_return_set, ret)
                    end
                    if not all_previous_offer_orig_reachable then
                        table.clear(offer_return_set)
                        break -- next offer
                    else
                        utils.add_full_table(return_set, offer_return_set)
                        table.clear(offer_return_set)
                    end
                end
            end
        end
    end
    if not offer_found then
        return {{own = card, path = {}}}
    elseif not worked_offer_found or not all_previous_offer_orig_reachable then
        return {}
    else
        return return_set
    end
end

local function get_path_set_ending_from_card(offer_set, card)
    -- return path_set: {{col, path}}, where path - ending of path from card to col

    local return_set = {}
    local offer_return_set = {}
    local ret = {}
    local offer_found = false
    local worked_offer_found  = false
    local all_next_offer_flip_reachable
    for oi, o in ipairs(offer_set) do
        for _, orig in ipairs(o.orig_set) do
            if orig == card then 
                offer_found = true
                if o.cancel ~= true then
                    worked_offer_found = true
                    all_next_offer_flip_reachable = true
                    for _, flip in ipairs(o.flip_set) do
                        ret = get_path_set_ending_from_card(offer_set, flip)
                        if #ret == 0 then
                            all_next_offer_flip_reachable = false
                            break -- next offer
                        end
                        for _, r in ipairs(ret) do
                            table.insert(r.path, oi)
                        end                    
                        utils.add_full_table(offer_return_set, ret)
                    end
                    if not all_next_offer_flip_reachable then
                        table.clear(offer_return_set)
                        break -- next offer
                    else
                        utils.add_full_table(return_set, offer_return_set)
                        table.clear(offer_return_set)
                    end
                end
            end
        end
    end
    if not offer_found then
        return {{col = card, path = {}}}
    elseif not worked_offer_found or not all_next_offer_flip_reachable then
        return {}
    else
        return return_set
    end
end

local function get_row_set_beginning_from_card(offer_set, card)
    -- return row_set: {{card_set, change_set}}, where change_set - beginning of path from own to card

    local return_set = {}
    local change_set = {}
    local card_set = {}
    local offer_found = false
    for oi, o in ipairs(offer_set) do
        for _, flip in ipairs(o.flip_set) do
            if flip == card then 
                offer_found = true
                for _, orig in ipairs(o.orig_set) do
                    local ret = {}
                    ret = get_row_set_beginning_from_card(offer_set, orig)
                    log.info('**** ret = '..json.encode(ret))
                    for _, r in ipairs(ret) do
                        if #r.change_set ~= 0 then
                            utils.add_full_table(change_set, r.change_set)
                        end
                        utils.add_full_table(card_set, r.card_set)
                    end          
                end
                table.insert(card_set, card)          
                table.insert(change_set, oi)          
                table.insert(return_set, {card_set = table.deepcopy(card_set), change_set = table.deepcopy(change_set)})
                log.info('**** return_set = '..json.encode(return_set))
                fiber.sleep(1)
                table.clear(change_set)
                table.clear(card_set)
            end
        end
    end
    if not offer_found then
        return {{card_set = {card}, change_set = {}}}
    else
        return return_set
    end
end

local function get_transfer_beginning(transfer_set, offer_index)
    -- return transfer_set: {{own, nil, path}}, where path - beginning of path from own to offer_index

    log.info('*@@* offer_index = '..tostring(offer_index))

    local return_set = {}
    local begin_set = {}
    for _, s in ipairs(transfer_set) do
        for j, o in ipairs(s.path) do
            if o == offer_index then
                table.move(s.path, 1, j-1, 1, begin_set)

                --log.info('*@@* o = '..tostring(o))
                --log.info('*@@* begin_set = '..json.encode(begin_set))

                -- Check for duplicates
                local found = false
                for i, e in ipairs(return_set) do
                    if e.own == s.own and utils.compare_tables(begin_set, e.path) then
                        found = true
                        break
                    end
                end
                if not found then
                    table.insert(
                        return_set, 
                        {
                            own = s.own,
                            col = nil,
                            path = table.deepcopy(begin_set)
                        }
                    )
                end

                --log.info('*@@* return_set = '..json.encode(return_set))

                table.clear(begin_set)
                break
            end
        end
    end
    return return_set
end

local function get_transfer_ending(transfer_set, offer_index)
    -- return transfer_set: {{nil, col, path}}, where path - endign of path from offer_index to col
    local return_set = {}
    local end_set = {}
    for _, s in ipairs(transfer_set) do
        for j, o in ipairs(s.path) do
            if o == offer_index then
                table.move(s.path, j+1, #s.path, 1, end_set)

                -- Check for duplicates
                local found = false
                for i, e in ipairs(return_set) do
                    if e.col == s.col and utils.compare_tables(end_set, e.path) then
                        found = true
                        break
                    end
                end
                if not found then
                    table.insert(
                        return_set, 
                        {
                            own = nil,
                            col = s.col,
                            path = table.deepcopy(end_set)
                        }
                    )
                end
                table.clear(end_set)
                break
            end
        end
    end
    return return_set
end

local function get_common_parent(offer_set, card_set, common_card_set)
    -- card_set - set of orig_set of initial offers
    -- common_card_set - stored set from previous steps
    -- Return found parent card or nil
    -- TODO: Need to fix func to support offers with one orig card and many flip
    if common_card_set == nil then
        common_card_set = {}
    end
    for _, c in ipairs(card_set) do
        for _, cc in ipairs(common_card_set) do
            if c == cc then
                return c
            end
        end
        table.insert(common_card_set, c)
    end
    for _, s in ipairs(offer_set) do
        for _, f in ipairs(s.flip_set) do 
            for _, c in ipairs(card_set) do
                if f == c then
                    for _, o in ipairs(s.orig_set) do
                        local result = get_common_parent(offer_set, {o}, common_card_set)
                        if result ~= nil then 
                            return result 
                        end
                    end
                end
            end            
        end
    end
    return nil
end

local function get_common_child(offer_set, card_set, common_card_set)
    -- card_set - set of flip_set of initial offers
    -- common_card_set - stored set from previous steps
    -- Return found child card or nil

    --log.info('*CC* card_set = '..json.encode(card_set))
    --fiber.sleep(1)

    if common_card_set == nil then
        common_card_set = {}
    end
    for _, c in ipairs(card_set) do
        for _, cc in ipairs(common_card_set) do
            if c == cc then
                return c
            end
        end
        table.insert(common_card_set, c)
    end
    for _, s in ipairs(offer_set) do
        for _, f in ipairs(s.orig_set) do 
            for _, c in ipairs(card_set) do
                if f == c then
                    for _, o in ipairs(s.flip_set) do
                        local result = get_common_child(offer_set, {o}, common_card_set)
                        if result ~= nil then 
                            return result 
                        end
                    end
                end
            end            
        end
    end
    return nil
end

local function has_canceled(offer_set, transfer_path)
    for _, p in ipairs(transfer_path) do
        for i, j in ipairs(offer_set) do
            if p == i and j.cancel == true then
                return true
            end
        end
    end
    return false
end

--[[ local function is_working_transfer_index_set(tranfer_set, offer_set)
    local result_set = {}
    for ti, t in ipairs(tranfer_set) do
        local found = false
        for _, o in ipairs(t.path) do
            if offer_set[o].cancel == true then
                found = true
                break
            end
        end
        if not found then
            table.insert(result_set, ti)
        end
    end
    return result_set
end ]]

local function resolve_graph(own_set, col_set, offer_set, available_index_set)
    -- check that all col_set cards can be obtainable
    -- return set of resolving pathes: {{resolving_offer_index_set}} or nil if graph is not resolved
    
    if available_index_set == nil then
        available_index_set = utils.get_index_set(offer_set)
    end
    
    --log.info('*RG* own_set = '..json.encode(own_set))
    --log.info('*RG* col_set = '..json.encode(col_set))
    --log.info('*RG* available_index_set = '..json.encode(available_index_set))

    local new_own_set = {}
    local new_col_set = {}
    local new_available_index_set = {}
    local result_set = {}
    local flip_found, offer_found
    for ai, a in ipairs(available_index_set) do

        --log.info('**** a = '..tostring(a))

        new_available_index_set = table.deepcopy(available_index_set)
        new_own_set = table.deepcopy(own_set)
        new_col_set = table.deepcopy(col_set)

        --log.info('**** new_own_set = '..json.encode(own_set))
        --log.info('**** new_col_set = '..json.encode(col_set))
        --log.info('**** new_available_index_set = '..json.encode(available_index_set))

        local all_orig_found = true
        for _, orig in ipairs(offer_set[a].orig_set) do
            
            --log.info('**** orig = '..tostring(orig))
            
            local orig_found = false
            for ci, c in ipairs(new_own_set) do

                --log.info('**** c = '..tostring(c))

                if  orig == base.get_burned_suit(c) or
                    orig == c
                then 
                    orig_found = true
                    if base.get_burned_suit(c) == c then
                        table.remove(new_own_set, ci)
                    end
                    break
                end
            end
            if not orig_found then
                all_orig_found = false
                break
            end
        end
        if all_orig_found then
            
            --log.info('**** orig new_own_set = '..json.encode(new_own_set))
            
            for _, flip in ipairs(offer_set[a].flip_set) do

                --log.info('**** flip = '..tostring(flip))

                flip_found = false
                for fi, f in ipairs(new_col_set) do

                    --log.info('**** f = '..tostring(f))

                    if flip == f then
                        flip_found = true
                        table.remove(new_col_set, fi) 
                        break
                    end
                end
                if not flip_found then
                    table.insert(new_own_set, flip)
                end

                --log.info('**** flip new_own_set = '..json.encode(new_own_set))
                --log.info('**** flip new_col_set = '..json.encode(new_col_set))
            end
            if #new_col_set == 0 then
                table.insert(result_set, {a})
            else
                table.remove(new_available_index_set, ai)
                if  #new_available_index_set == 0 and
                    #new_own_set ~= 0 and
                    #new_col_set ~= 0
                then

                    --log.info('*rr* Implicit')
                    local found
                    for _, o in ipairs(new_own_set) do
                        found = false
                        for _, c in ipairs(new_col_set) do
                            if base.get_burned_suit(o) == c then
                                found = true
                                break
                            end
                        end
                        if not found then
                            break
                        end
                    end
                    if found then
                        table.insert(result_set, {a})
                    end
                else
                    local result = resolve_graph(new_own_set, new_col_set, offer_set, new_available_index_set)
                    for _, r in ipairs(result) do
                        table.insert(r, 1, a)
                        table.insert(result_set, r)
                        --log.info('**** result_set = '..json.encode(result_set))
                    end
                end
            end
        end
    end    
    --log.info('*EE* result_set = '..json.encode(result_set))
    --fiber.sleep(1)
    return result_set -- can return {}
end

local function build_line_set(offer_set, available_index_set, card, line_set)
    -- build line_set: {card_set, change_set} for all available lines
    -- return set of resolving pathes: {{resolving_offer_index_set}} or nil if graph is not resolved

    log.info('**** offer_set = '..json.encode(offer_set))
    log.info('**** available_index_set = '..json.encode(available_index_set))
    log.info('**** card = '..tostring(card))
    log.info('**** line_set = '..json.encode(line_set))
    fiber.sleep(1)

    if available_index_set == nil then
        available_index_set = utils.get_index_set(offer_set)
    end

    if line_set == nil then
        line_set = {}
    end
    
    local change_set = {}
    local card_set = {}
    for ai, a in ipairs(available_index_set) do
        if card == nil then
            table.insert(change_set, a)
            table.remove(available_index_set, ai)
            for _, orig in ipairs(offer_set[a].orig_set) do 
                table.insert(card_set, orig)
                if #available_index_set > 0 then
                    build_line_set(offer_set, available_index_set, orig, line_set)
                end
            end
            for _, flip in ipairs(offer_set[a].flip_set) do
                table.insert(card_set, flip)
                if #available_index_set > 0 then
                    build_line_set(offer_set, available_index_set, flip, line_set)
                end
            end
            table.insert(line_set, {card_set = table.deepcopy(card_set), change_set = table.deepcopy(change_set)})

            log.info('**** line_set = '..json.encode(line_set))
            fiber.sleep(1)

            table.clear(change_set)
            table.clear(card_set)
        else
            for _, orig in ipairs(offer_set[a].orig_set) do
                if orig == card then 
                    table.insert(change_set, a)
                    table.remove(available_index_set, ai)
                    for _, flip in ipairs(offer_set[a].flip_set) do
                        table.insert(card_set, flip)
                        if #available_index_set > 0 then
                            build_line_set(offer_set, available_index_set, flip, line_set)                                  
                        end
                    end
                    table.insert(line_set, {card_set = table.deepcopy(card_set), change_set = table.deepcopy(change_set)})
                    table.clear(change_set)
                    table.clear(card_set)
                end
            end
            for _, flip in ipairs(offer_set[a].flip_set) do
                if flip == card then 
                    table.insert(change_set, a)
                    table.remove(available_index_set, ai)
                    for _, flip in ipairs(offer_set[a].flip_set) do
                        table.insert(card_set, flip)
                        if #available_index_set > 0 then
                            build_line_set(offer_set, available_index_set, flip, line_set)                                  
                        end
                    end
                    table.insert(line_set, {card_set = table.deepcopy(card_set), change_set = table.deepcopy(change_set)})
                    table.clear(change_set)
                    table.clear(card_set)
                end
            end
        end
    end
    return line_set
end

local function trace(func, tbl, topic)
    -- func     -- func name: string, mandatory
    -- tbl      -- structure for log: string/map/array of map, mandatory
                -- string could be logged only when user_uuid_for_trace is nil
                -- when user_uuid_for_trace or topic_for_trace is not nil, standalone map or map in array
                --      should contain user_uuid field
    -- topic    -- mark of trace point: string

    local f = debug.getinfo(2)
    log.info('**** func = '..tostring(f))
    log.info('**** func = '..tostring(f.name))
    log.info('**** func = '..tostring(f.what))
    log.info('**** func = '..tostring(f.namewhat))
    log.info('**** func = '..tostring(f.short_src))
    log.info('**** func = '..tostring(debug.traceback()))
    
    fiber.sleep(1)
end

local function create_fake_incoming_branches(own_set, col_set, offer_set, transfer_set, fi_count, fi_out_index, fi_in_index, fi_alt_offer)
    -- offer_set and transfer_set will be modified!

    local incoming_offer_index, outgoing_offer_index
    for s = 1, fi_count do

        log.info('**** s = '..tostring(s))

        local all_checks_done = false
        for try = 1, TRY_COUNT do

            if fi_out_index == nil or
                (type(fi_out_index) == 'table' and fi_out_index[s] == nil)
            then
                outgoing_offer_index = math.random(#offer_set)
            elseif type(fi_out_index) == 'number' then
                outgoing_offer_index = fi_out_index
            else
                outgoing_offer_index = fi_out_index[s]
            end

            if fi_in_index == nil or
                (type(fi_in_index) == 'table' and fi_in_index[s] == nil)
            then
                incoming_offer_index = math.random(#offer_set)
            elseif type(fi_in_index) == 'number' then
                incoming_offer_index = fi_in_index
            else
                incoming_offer_index = fi_in_index[s]
            end

            log.info('**** outgoing_offer_index = '..tostring(outgoing_offer_index))
            log.info('**** incoming_offer_index = '..tostring(incoming_offer_index))

            if incoming_offer_index ~= outgoing_offer_index then

                -- Check that branch has not already exist
                local branch_already_exists = false
                for _, i in ipairs(offer_set[incoming_offer_index].flip_set) do
                    for _, j in ipairs(offer_set[outgoing_offer_index].orig_set) do
                        if i == j then 
                            branch_already_exists = true
                            break
                        end
                    end
                    if branch_already_exists then
                        break
                    end
                end
                
                if branch_already_exists == false then

                    -- Check that incoming flip_set not included col
                    local col_found = false
                    for _, f in ipairs(offer_set[incoming_offer_index].flip_set) do
                        for _, c in ipairs(col_set) do
                            if f == c then
                                col_found = true
                                break
                            end
                        end
                        if col_found then
                            break
                        end
                    end

                    if col_found == false then

                        -- Check for same flip cards (both offers finish to same node)
                        local same_flip_found = false
                        for _, oc in ipairs(offer_set[outgoing_offer_index].flip_set) do
                            for _, ic in ipairs(offer_set[incoming_offer_index].flip_set) do
                                if oc == ic then
                                    same_flip_found = true
                                    break
                                end
                            end
                            if same_flip_found then
                                break
                            end
                        end

                        if same_flip_found == false then

                            -- Check that outgoing offer reachable
                            local outgoing_path_set = {}
                            local outgoing_offer_reachable = true
                            for _, flip in ipairs(offer_set[outgoing_offer_index].flip_set) do
                                outgoing_path_set = get_path_set_beginning_from_card(offer_set, flip)
                                if #outgoing_path_set == 0 then
                                    outgoing_offer_reachable = false
                                    break
                                end
                            end

                            if outgoing_offer_reachable then -- TODO: change to start test from orig_set + cancel flag for offer
                                
                                -- Check that incoming offer reachable
                                local incoming_path_set = {}
                                local incoming_offer_reachable = true
                                for _, flip in ipairs(offer_set[incoming_offer_index].flip_set) do
                                    incoming_path_set = get_path_set_beginning_from_card(offer_set, flip)
                                    if #incoming_path_set == 0 then
                                        incoming_offer_reachable = false
                                        break
                                    end
                                end

                                if incoming_offer_reachable then                            

                                    -- Check for same parents
                                    local both_parent_cards = {}
                                    utils.add_full_table(both_parent_cards, offer_set[outgoing_offer_index].orig_set)
                                    utils.add_full_table(both_parent_cards, offer_set[incoming_offer_index].orig_set)
                                    local same_parent_card = get_common_parent(offer_set, both_parent_cards)         

                                    if same_parent_card == nil then                

                                        -- Check for same children
                                        local both_child_cards = {}
                                        utils.add_full_table(both_child_cards, offer_set[outgoing_offer_index].flip_set)
                                        utils.add_full_table(both_child_cards, offer_set[incoming_offer_index].flip_set)
                                        local same_child_card = get_common_child(offer_set, both_child_cards)
                                        if same_child_card == nil then
                                            -- All checks done
                                            all_checks_done = true
                                            break
                                        end
                                    end
                                end
                            end
                        end                    
                    end
                end
            end
        end

        if all_checks_done then

            log.info('**** ALL CHECKS DONE')

            -- Find all col_cards from transfers with outgoing offer
            local outgoing_col_set = {} -- {col, transfer_index_set = {}}
            for ki, k in ipairs(transfer_set) do
                if not has_canceled(offer_set, k.path)then
                    for _, l in ipairs(k.path) do
                        if l == outgoing_offer_index then
                            local found = false
                            for _, m in ipairs(outgoing_col_set) do
                                if k.col == m.col then
                                    table.insert(m.transfer_index_set, ki)
                                    found = true
                                    break
                                end
                            end
                            if found == false then
                                table.insert(outgoing_col_set, {col = k.col, transfer_index_set = {ki}, alt_transfer_index_set = {}})
                            end
                        end
                    end
                end
            end

            log.info('**** outgoing_col_set = '..json.encode(outgoing_col_set))

            -- Find another transfers with same col_cards
            for ki, k in ipairs(transfer_set)do
                if not has_canceled(offer_set, k.path) then
                    for _, m in ipairs(outgoing_col_set)do
                        if k.col == m.col then
                            local found = false
                            for _, n in ipairs(m.transfer_index_set) do
                                if ki == n or
                                    utils.has_same(transfer_set[ki].path, transfer_set[n].path) 
                                then
                                    found = true
                                end
                            end
                            if not found then
                                table.insert(m.alt_transfer_index_set, ki)                                
                            end
                        end
                    end
                end
            end
            
            log.info('**** outgoing_col_set = '..json.encode(outgoing_col_set))

            -- Check unique transfer paths
            local unique_outgoing_found = false
            for _, t in ipairs(outgoing_col_set) do
                if #t.alt_transfer_index_set == 0 then
                    unique_outgoing_found = true
                    break
                end
            end

            local unique_offer_found = true -- Flag for all kind of unique check below

            local incoming_card = offer_set[incoming_offer_index].flip_set[
                math.random(#offer_set[incoming_offer_index].flip_set)
            ]

            log.info('**** incoming_card = '..tostring(incoming_card))

            if unique_outgoing_found then       
                -- TODO: rewrite section with grouping around col_card?
                
                log.info('**** unique_outgoing found')

                -- Find all transfer paths for all cards from beginning to incoming offer 
                local incoming_transfer_index_set = {}
                local other_transfer_index_set = {}
                local unique_transfer_index_set = {}
                incoming_transfer_index_set = get_all_beginning_from_card(transfer_set, offer_set, incoming_card)
                other_transfer_index_set = utils.exclude_table(utils.get_index_set(transfer_set), incoming_transfer_index_set)

                log.info('**** incoming_transfer_index_set = '..json.encode(incoming_transfer_index_set))
                log.info('**** other_transfer_index_set = '..json.encode(other_transfer_index_set))

                -- Choose unique_transfer_index_set
                for _, i in ipairs(incoming_transfer_index_set) do
                    if not has_canceled(offer_set, transfer_set[i].path) then
                        local found = false
                        for _, j in ipairs(other_transfer_index_set) do
                            if not has_canceled(offer_set, transfer_set[j].path) then
                                if transfer_set[i].col == transfer_set[j].col then

                                    -- Check that transfers is not intersect
                                    if not utils.has_same(transfer_set[i].path, transfer_set[j].path) then
                                        found = true
                                        break
                                    end
                                end
                            end
                        end
                        if found == false then
                            table.insert(unique_transfer_index_set, i)
                        end
                    end
                end

                log.info('**** unique_transfer_index_set = '..json.encode(unique_transfer_index_set))

                if #unique_transfer_index_set > 0 then

                    local choosing_transfer_index_set = {}
                    local temp_transfer_index_set = utils.exclude_table(utils.get_index_set(transfer_set), unique_transfer_index_set)
                    for _, k in ipairs(temp_transfer_index_set) do
                        if not has_canceled(offer_set, transfer_set[k].path) then
                            local found = false
                            for _, l in ipairs(unique_transfer_index_set) do
                                
                                -- Check that transfers is not intersect
                                if not utils.has_same(transfer_set[k].path, transfer_set[l].path) then
                                    found = true
                                    break
                                end
                            end
                            if found then
                                table.insert(choosing_transfer_index_set, k)
                            end
                        end
                    end

                    log.info('**** choosing_transfer_index_set = '..json.encode(choosing_transfer_index_set))

                    -- Choose col_card which need new offers
                    local unique_col_set = {}
                    for _, i in ipairs(unique_transfer_index_set) do
                        local found = false
                        for _, j in ipairs(unique_col_set) do
                            if transfer_set[i].col == j then
                                found = true
                                break
                            end
                        end
                        if not found then
                            table.insert(unique_col_set, transfer_set[i].col)
                        end
                    end

                    log.info('**** unique_col_set = '..json.encode(unique_col_set))

                    for _, i in ipairs(unique_col_set) do

                        -- Looking for offer which is in transfer_set but not in unique_incoming
                        local offer_index
                        if fi_alt_offer == nil or
                            (type(fi_alt_offer) == 'table' and fi_alt_offer[s] == nil)
                        then
                            for try = 1, TRY_COUNT do                                
                                local transfer_idx = choosing_transfer_index_set[
                                    math.random(#choosing_transfer_index_set)
                                ]                            
                                local path_idx = math.random(
                                    #transfer_set[
                                        transfer_idx
                                    ].path
                                )
                                offer_index = transfer_set[transfer_idx].path[path_idx]
                                
                                if offer_index > 0 then
                                    break
                                end
                            end
                        elseif type(fi_alt_offer) == 'number' then
                            offer_index = fi_alt_offer
                        else
                            offer_index = fi_alt_offer[s]
                        end
                                    
                        log.info('**** offer_index = '..tostring(offer_index))

                        if offer_index > 0 then

                            -- Add offer branch and new transfers
                            table.insert(
                                offer_set[offer_index].flip_set, 
                                i
                            )
                            local beginning_set = {}
                            local ending_set = {}
                            local path = {}
                            beginning_set = get_transfer_beginning(transfer_set, offer_index)
                            log.info('*##* beginning_set = '..json.encode(beginning_set))
                            for _, b in ipairs(beginning_set) do
                                utils.add_full_table(path, b.path)
                                table.insert(path, offer_index)
    
                                table.insert(
                                    transfer_set,
                                    {
                                        own = b.own,
                                        col = i,
                                        path = table.deepcopy(path)
                                    }
                                )
                                table.clear(path)
                            end

                            log.info('*##* offer_set = '..json.encode(offer_set))
                            log.info('*##* transfer_set = '..json.encode(transfer_set))
                        else
                            unique_offer_found = false
                        end
                    end

                    if unique_offer_found then

                        -- Cancel offers on the old path 
                        local parent_card
                        local ending_set = {}
                        for _, i in ipairs(unique_col_set) do

                            parent_card = get_common_parent(
                                offer_set, 
                                utils.add_table({i}, {incoming_card})
                            )

                            log.info('**** parent_card = '..json.encode(parent_card))

                            ending_set = get_path_set_ending_from_card(offer_set, parent_card)

                            log.info('**** ending_set = '..json.encode(ending_set))

                            for _, p in ipairs(ending_set) do
                                if p.col == i then
                                    for _, o in ipairs(p.path) do
                                        offer_set[o].cancel = true
                                    end
                                end
                            end                            
                        end
                    end

                    log.info('#### offer_set = '..json.encode(offer_set))
                    log.info('#### transfer_set = '..json.encode(transfer_set))
                end
            end

            if unique_offer_found then
                table.insert(
                    offer_set[outgoing_offer_index].orig_set, 
                    incoming_card
                )
                local beginning_set = {}
                local ending_set = {}
                local return_set = {}
                local path = {}
                beginning_set = get_path_set_beginning_from_card(offer_set, incoming_card)
                --ending_set = get_transfer_ending(transfer_set, outgoing_offer_index)
                for _, flip in ipairs(offer_set[outgoing_offer_index].flip_set) do
                    return_set = get_path_set_ending_from_card(offer_set, flip)

                    log.info('**** return_set = '..json.encode(return_set))

                    for _, r in ipairs(return_set) do
                        for _, c in ipairs(col_set) do 
                            if r.col == c then
                                table.insert(ending_set, r)
                            end
                        end
                    end
                end

                log.info('**** beginning_set = '..json.encode(beginning_set))
                log.info('**** ending_set = '..json.encode(ending_set))

                for _, b in ipairs(beginning_set) do
                    for _, e in ipairs(ending_set) do
                        utils.add_full_table(path, b.path)
                        table.insert(path, outgoing_offer_index)
                        utils.add_full_table(path, e.path)
                        table.insert(
                            transfer_set,
                            {
                                own = b.own,
                                col = e.col,
                                path = table.deepcopy(path)
                            }
                        )
                        table.clear(path)
                    end                
                end

                log.info('**** offer_set = '..json.encode(offer_set))
                log.info('**** transfer_set = '..json.encode(transfer_set))

            end
        end
    end 
    return {
        col_set = col_set,
        own_set = own_set,
        offer_set = offer_set,
        transfer_set = transfer_set
    }
end

local function create_study_level(level, balance, own_set)
    local col_set = {}
    local offer_set = {}
    local moves
    local card1, card2, card3, card4
    if level == 1 then
        -- Lesson 1: simplest party with one exchange
        card1 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE)
        card2 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1})

        if card1 == nil or card2 == nil then
            return nil, base.GameError:new('NOT_ENOUGH_CARDS_FOR_FIRST_LEVEL')
        end

        col_set = {card1}
        own_set = {card2}
        offer_set = {
            {orig_set = {card2}, flip_set = {card1}}
        }
        moves = 1
        balance = 0

    elseif level == 2 then
        -- Lesson 2: several exchanges
        card1 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE)
        card2 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1})
        card3 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1, card2})
        card4 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1, card2, card3})

        if card1 == nil or card2 == nil or card3 == nil or card4 == nil then
            return nil, base.GameError:new('NOT_ENOUGH_CARDS_FOR_STUDY_LEVEL')
        end

        col_set = {card1}
        own_set = {card2}
        offer_set = {
            {orig_set = {card2}, flip_set = {card3}},
            {orig_set = {card2}, flip_set = {card4}},
            {orig_set = {card3}, flip_set = {card1}}
        }
        moves = 2
        balance = 0

    elseif level == 3 then
        -- Lesson 3: coins!
        card1 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE)
        card2 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1})
        card3 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1, card2})
        card4 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1, card2, card3})

        if card1 == nil or card2 == nil or card3 == nil or card4 == nil then
            return nil, base.GameError:new('NOT_ENOUGH_CARDS_FOR_STUDY_LEVEL')
        end

        col_set = {card1, card4}
        own_set = {card2, card3}
        offer_set = {
            {orig_set = {card2}, flip_set = {card1, base.COIN}},
            {orig_set = {card3, base.COIN}, flip_set = {card4}}
        }
        moves = 2
        balance = 0

    elseif level == 4 then
        -- Lesson 4: persistent cards
        card1 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE)
        card2 = base.get_card_by_suit(base.get_persistent_suit(card1))
        card3 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1, card2})
        card4 = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, {card1, card2, card3})

        if card1 == nil or card2 == nil or card3 == nil or card4 == nil then
            return nil, base.GameError:new('NOT_ENOUGH_CARDS_FOR_STUDY_LEVEL')
        end

        col_set = {card1, card4}
        own_set = {card2, card3}
        offer_set = {
            {orig_set = {card1, card3}, flip_set = {card4}}
        }
        moves = 1
        balance = 0

    else
        return nil, base.GameError:new('INCORRECT_LEVEL')
    end

    return {
        col_set = col_set,
        own_set = own_set,
        offer_set = offer_set,
        moves = moves,
        balance = balance
    }
end

local function create_level_old(level, balance, own_set)
    if level <= 4 then
        local tbl = {}
        local e
        tbl, e = create_study_level(level, balance, own_set)
        return tbl, e
    end

    -- Insert cards from own_set
    local col_set = {}
    local transfer_from_set, transfer_to_set, e
    local transfer_count = math.min(config.get_col_count(level), config.get_own_count(level)) - config.get_distinct_count(level)
    if transfer_count > 0 then
        transfer_from_set, e = base.choose_random_card_set(own_set, {}, transfer_count)
        if e then
            return nil, e
        end
        transfer_to_set = base.get_burned_set(transfer_from_set)
        col_set = table.deepcopy(transfer_to_set)
    end

    -- Expand col_set to required count
    utils.add_full_table(
        col_set, 
        base.get_unique_card_set(
            base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, 
            base.get_burned_set(own_set), 
            config.get_col_count(level) - #col_set
        )
    )

    -- Expand own_set to required count
    local required_own_count = config.get_own_count(level) - #own_set
    if required_own_count > 0 then
        local exclude_set = table.deepcopy(col_set)
        utils.add_full_table(exclude_set, base.get_burned_set(own_set))
        utils.add_full_table(own_set, base.get_unique_card_set(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, exclude_set, required_own_count))
    end

    -- Choose pairs without duplicates on other side
    local offer_set = {}
    local moves
    local own_for_offer_set, col_for_offer_set, e 
    own_for_offer_set, e = base.choose_random_card_set(own_set, transfer_from_set, config.get_distinct_count(level))
    if e then
        return nil, e
    end
    own_for_offer_set = base.get_burned_set(own_for_offer_set)
    col_for_offer_set = base.choose_random_card_set(col_set, transfer_to_set, config.get_distinct_count(level))

    -- Get additional cards for intermediate moves
    local index_for_intermediate = {}
    local card_for_intermediate = {}
    local ex_set = {}
    utils.add_full_table(ex_set, own_for_offer_set)
    utils.add_full_table(ex_set, col_for_offer_set)
    if config.get_move_count(level) <= config.get_distinct_count(level) then
        moves = 0
    else
        moves = config.get_move_count(level)
        for i = 1, (moves - config.get_distinct_count(level)) do
            table.insert(index_for_intermediate, math.random(config.get_distinct_count(level)))
            local c, e = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, ex_set)
            if e then
                return nil, e
            end
            table.insert(card_for_intermediate, c)
            table.insert(ex_set, c)
        end
    end

    -- Structure for transfer_set: {own, col, path}, where path = set of offer_ids
    local transfer_set = {}
    local path = {}
    local own, col

    -- Build offer_set
    local orig_set = {}
    local flip_set = {}
    for i = 1, config.get_distinct_count(level) do
        own = own_for_offer_set[i]
        col = col_for_offer_set[i]
        table.insert(orig_set, own)
        for j = 1, (moves - config.get_distinct_count(level)) do
            if index_for_intermediate[j] == i then
                table.insert(flip_set, card_for_intermediate[j])
                table.insert(offer_set, {orig_set = table.deepcopy(orig_set), flip_set = table.deepcopy(flip_set)})
                table.insert(path, #offer_set)
                table.clear(orig_set)
                table.clear(flip_set)
                table.insert(orig_set, card_for_intermediate[j])
            end
        end
        table.insert(flip_set, col)
        table.insert(offer_set, {orig_set = table.deepcopy(orig_set), flip_set = table.deepcopy(flip_set)})
        table.insert(path, #offer_set)
        table.insert(transfer_set, {own = own, col = col, path = table.deepcopy(path)})
        table.clear(orig_set)
        table.clear(flip_set)
        table.clear(path)
    end
    
    -- Get cards for fake offers
    local index_for_fake_offer = {}
    local card_for_fake_offer = {}
    for i = 1, config.get_fake_offer_count(level) do
        table.insert(index_for_fake_offer, math.random(#offer_set))
        while true do
            local c, e = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, ex_set)
            if e then
                return nil, e
            end    
            local found = false
            for j = 1, i - 1 do
                if card_for_fake_offer[j] == card_for_fake_offer[i] and
                    index_for_fake_offer[j] == index_for_fake_offer[i]
                then
                   found = true  
                   break
                end   
            end    
            if not found then
                table.insert(card_for_fake_offer, c)
                --table.insert(ex_set, c)
                break
            end    
        end    
    end    

    -- Add fake offers
    local correct_offer_count = #offer_set
    local added_index = {}
    local path_set = {}
    for i = 1, correct_offer_count do
        for j = 1, config.get_fake_offer_count(level) do
            if index_for_fake_offer[j] == i then
                local card = offer_set[i].orig_set[1]
                for k = j-1, 1, -1 do
                    if index_for_fake_offer[k] == index_for_fake_offer[j] then
                        card = offer_set[added_index[k] ].flip_set[1]
                        break
                    end
                end
                table.insert(orig_set, card)
                table.insert(flip_set, card_for_fake_offer[j])
                table.insert(offer_set, {orig_set = table.deepcopy(orig_set), flip_set = table.deepcopy(flip_set)})
                added_index[j] = #offer_set
                table.clear(orig_set)
                table.clear(flip_set)
            end
        end
    end

    -- Add fake incoming branches
    local fi_count = config.get_fake_incoming_prob(level)
    create_fake_incoming_branches(own_set, col_set, offer_set, transfer_set, fi_count)

    -- Add additional fake cards to offers
    --[[ for i = 1, #offer_set do
        local p = math.random()
        if p < fake_card_prob[level] then
            local c, e = get_card(BURNED, ORDINARY, RARITY_SIMPLE)
            if e then
                return nil, e
            end ]]
            --[[ if p < fake_card_prob[level]/2 then
                table.insert(offer_set[i].original_set, c)
            else ]]
                --table.insert(offer_set[i].flip_set, c)
            --end
        --[[ end
    end ]]

    -- Mix offers

    return {
        col_set = col_set,
        own_set = own_set,
        offer_set = offer_set,
        moves = moves,
        balance = balance
    }
end

local function create_level(level, balance, own_set)
    if level <= 4 then
        local tbl = {}
        local e
        tbl, e = create_study_level(level, balance, own_set)
        return tbl, e
    end

    -- Insert cards from own_set
    local col_set = {}
    local transfer_from_set, transfer_to_set, e
    local transfer_count = math.min(config.get_col_count(level), config.get_own_count(level)) - config.get_distinct_count(level)
    if transfer_count > 0 then
        transfer_from_set, e = base.choose_random_card_set(own_set, {}, transfer_count)
        if e then
            return nil, e
        end
        transfer_to_set = base.get_burned_set(transfer_from_set)
        col_set = table.deepcopy(transfer_to_set)
    end

    -- Expand col_set to required count
    utils.add_full_table(
        col_set, 
        base.get_unique_card_set(
            base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, 
            base.get_burned_set(own_set), 
            config.get_col_count(level) - #col_set
        )
    )

    -- Expand own_set to required count
    local required_own_count = config.get_own_count(level) - #own_set
    if required_own_count > 0 then
        local exclude_set = table.deepcopy(col_set)
        utils.add_full_table(exclude_set, base.get_burned_set(own_set))
        utils.add_full_table(own_set, base.get_unique_card_set(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, exclude_set, required_own_count))
    end

    -- Choose pairs without duplicates on other side
    local offer_set = {}
    local moves
    local own_for_offer_set, col_for_offer_set, e 
    own_for_offer_set, e = base.choose_random_card_set(own_set, transfer_from_set, config.get_distinct_count(level))
    if e then
        return nil, e
    end
    own_for_offer_set = base.get_burned_set(own_for_offer_set) -- TODO: move ^
    col_for_offer_set = base.choose_random_card_set(col_set, transfer_to_set, config.get_distinct_count(level))

    -- Get additional cards for intermediate moves
    local index_for_intermediate = {}
    local card_for_intermediate = {}
    local ex_set = {}
    utils.add_full_table(ex_set, own_for_offer_set)
    utils.add_full_table(ex_set, col_for_offer_set)
    if config.get_move_count(level) <= config.get_distinct_count(level) then
        moves = 0
    else
        moves = config.get_move_count(level)
        for i = 1, (moves - config.get_distinct_count(level)) do
            table.insert(index_for_intermediate, math.random(config.get_distinct_count(level)))
            local c, e = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, ex_set)
            if e then
                return nil, e
            end
            table.insert(card_for_intermediate, c)
            table.insert(ex_set, c)
        end
    end

    -- Structure for transfer_set: {own, col, path}, where path = set of offer_ids
    local transfer_set = {}
    local path = {}
    local own, col

    -- Structure for row_set: {card_set, change_set}, where each row is full number of cards and offer for converting from own to col
    local row_set = {}
    local card_set = {}
    local change_set = {}

    -- Build offer_set
    local orig_set = {}
    local flip_set = {}
    for i = 1, config.get_distinct_count(level) do
        own = own_for_offer_set[i]
        col = col_for_offer_set[i]
        table.insert(orig_set, own)
        table.insert(card_set, own) --
        for j = 1, (moves - config.get_distinct_count(level)) do
            if index_for_intermediate[j] == i then
                table.insert(flip_set, card_for_intermediate[j])
                table.insert(offer_set, {orig_set = table.deepcopy(orig_set), flip_set = table.deepcopy(flip_set)})
                table.insert(path, #offer_set)
                table.insert(card_set, card_for_intermediate[j]) --
                table.insert(change_set, #offer_set) --
                table.clear(orig_set)
                table.clear(flip_set)
                table.insert(orig_set, card_for_intermediate[j])
            end
        end
        table.insert(flip_set, col)
        table.insert(offer_set, {orig_set = table.deepcopy(orig_set), flip_set = table.deepcopy(flip_set)})
        table.insert(path, #offer_set)
        table.insert(transfer_set, {own = own, col = col, path = table.deepcopy(path)})
        table.insert(card_set, col) --
        table.insert(change_set, #offer_set) --
        table.insert(row_set, {card_set = table.deepcopy(card_set), change_set = table.deepcopy(change_set)}) --
        table.clear(orig_set)
        table.clear(flip_set)
        table.clear(path)
        table.clear(card_set) --
        table.clear(change_set) --
    end
    
    -- Get cards for fake offers
    local index_for_fake_offer = {}
    local card_for_fake_offer = {}
    for i = 1, config.get_fake_offer_count(level) do
        table.insert(index_for_fake_offer, math.random(#offer_set))
        while true do
            local c, e = base.get_card(base.BURNED, base.ORDINARY, base.RARITY_SIMPLE, ex_set)
            if e then
                return nil, e
            end    
            local found = false
            for j = 1, i - 1 do
                if card_for_fake_offer[j] == card_for_fake_offer[i] and -- TODO: check can it be true?
                    index_for_fake_offer[j] == index_for_fake_offer[i]
                then
                   found = true  
                   break
                end   
            end    
            if not found then
                table.insert(card_for_fake_offer, c)
                table.insert(ex_set, c) -- Uncommented 17.07.2023
                break
            end    
        end    
    end    

    -- Add fake offers
    local correct_offer_count = #offer_set
    local added_index = {}
    for i = 1, correct_offer_count do
        for j = 1, config.get_fake_offer_count(level) do
            if index_for_fake_offer[j] == i then
                local card = offer_set[i].orig_set[1]
                for k = j-1, 1, -1 do
                    if index_for_fake_offer[k] == index_for_fake_offer[j] then
                        card = offer_set[added_index[k]].flip_set[1]
                        break
                    end
                end
                table.insert(orig_set, card)
                table.insert(flip_set, card_for_fake_offer[j])
                table.insert(offer_set, {orig_set = table.deepcopy(orig_set), flip_set = table.deepcopy(flip_set)})
                added_index[j] = #offer_set
                table.clear(orig_set)
                table.clear(flip_set)
            end
        end
    end

    -- Add fake incoming branches
    local fi_count = config.get_fake_incoming_prob(level)
    create_fake_incoming_branches(own_set, col_set, offer_set, transfer_set, fi_count)

    -- Add additional fake cards to offers
    --[[ for i = 1, #offer_set do
        local p = math.random()
        if p < fake_card_prob[level] then
            local c, e = get_card(BURNED, ORDINARY, RARITY_SIMPLE)
            if e then
                return nil, e
            end ]]
            --[[ if p < fake_card_prob[level]/2 then
                table.insert(offer_set[i].original_set, c)
            else ]]
                --table.insert(offer_set[i].flip_set, c)
            --end
        --[[ end
    end ]]

    -- Mix offers

    return {
        col_set = col_set,
        own_set = own_set,
        offer_set = offer_set,
        moves = moves,
        balance = balance
    }
end

return{
    create_level = create_level,
    create_fake_incoming_branches = create_fake_incoming_branches,
    resolve_graph = resolve_graph,
    build_line_set = build_line_set, 
    get_path_set_beginning_from_card = get_path_set_beginning_from_card,
    get_row_set_beginning_from_card = get_row_set_beginning_from_card
}