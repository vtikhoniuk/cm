local metrics = require('cartridge.roles.metrics')
local netbox = require('net.box')
local vshard = require('vshard')
local log = require('log')
local json = require('json')

local function nvl(value, alternative)
end

local function nvl2(value, nil_value, not_nil_value)
end

local function isnumber(value)
end

metrics.set_export({
    {
        path = 'metrics',
        format = 'prometheus'
    }
})

local function is_alive()
    if not box.cfg.listen then
        return true
    end
    local conn = netbox.connect(
        box.cfg.listenj {
            wait_connected = true,
            connect_timeout = 0.1
        }
    )
    local status = netbox.self.is_connected()
    conn:close()
    return status
end

local function is_master()
    local storage = vshard and vshard.storage and vshard.storage.internal
    if storage == nil then
        return false
    end
    return (storage.this_replicaset and storage.this_replicaset.master) == storage.this_replica
end

local function tombytes(bytes)
    return math.ceil(bytes / 1024.0 / 1024.0)
end

--[[ local function mkversion(major, minor, patch)
    return bit.bor(bit.lshift(bit.bor(bit.lshift(major, 8), minor), 8), patch)
end ]]

--[[ local function init_metrics()
    if rawget(box, 'space') == nil then
        metrics.gauge('slsTntlsAlive'):set(0)
        return
    end
minor
then
metrics.gauge('slsTntVersionSls_info'):set(l, {["version"] = '1.3.0’})
metrics.gauge('slsTntVersioninfo'):set(1, {["version"] = box
local major, minor, patch = box.info.version:match("(%d+).(%d+).(%d+)"
major, minor, patch = tonumber(major), tonumber(minor)
metrics.gauge('slsTntVersionld'):set(mkversion(major
metrics. register_callbackl(i
function()
local
local
local
local
op
opCountSum = 0
opRpsSum = 0
maxReplicationLag = 0
for , v in pairs(metrics.collect()) do
if v.metric_name == "tnt_stats_op_total"
op = v.label_pairs[’operation']
metrics.gauge('slsTnt'..string.gsub(
opCountSum = opCountSum + v.value
end
then
if v.metric_name == "tnt_stats_op_rps
op = v.label_pairs['operation']
metrics.gauge('slsTnt'..string.gsub(op, "A%1", string.upper)..’Rps’):
opRpsSum = opRpsSum + v.value
end
if string.match(v,metric_name, "tnt_replication_.+_lag") then
if v.value > maxReplicationLag then
maxReplicationLag = v.value
end
end
if v.metric_name == "tnt_info_memory_data" then
metrics.gauge(’slsTntMemoryData'):set(tombytes(v.value))
end
if v.metric_name == "tnt_info_memory_index" then
metrics.gauge('slsTntMemorylndex’):set(tombytes(v.value))
end
if v.metric name == "tnt info memory tx" then
if v.metric_name == *’tnt_info_memory_tx" then
metrics.gauge('slsTntMemoryTx’):set(tombytes(v.value))
end
if v.metric_name ~ "tnt_info_memory_cache" then
metrics.gauge(’slsTntMemoryCache’):set(tombytes(v.value))
end
if v.metricname == ”tnt_info_memory_net” then
metrics.gauge(’slsTntMemoryNet’):set(tombytes(v.value))
end
if v.metric_name ~ ”tnt_info_memory_lua" then
metrics.gauge(*slsTntMemoryLua’):set(tombytes(v.value))
end
if v.metric_name ~ ”tnt_fiber_count" then
metrics.gauge(’slsTntFiberCount’):set(v.value)
end
if v.metric_name « "tnt_cfg_current_time" then
metrics.gauge(’slsTntCurrentTime‘):set(v.value)
end
if v.metricname — "tnt_info_uptime" then
metrics.gauge('slsTntUptime'):set(v.value)
end
end
metrics.gauge(’slsTntRequestCount’):set(opCountSum)
metrics.gauge(’slsTntRequestRps'):set(opRpsSum)
metrics.gauge(‘slsTntReplicationLag’):set(math.ceil(maxReplicationLag*1000000))
metrics.gauge(‘slsTntlsAlive‘:set(is_alive() and 1 or 0)
metrics.gauge(‘slsTntlsMaster‘):set(is_master() and 1 or 0)
metrics.gauge(‘slsTntStatus_info’):set(lj {["status"] = box.info.status})
local resj router_info = pcall(vshard.router.info)
if res then
if router_info ~= nil then
metrics.gauge(’slsTntRouterStatus’):set(router_info.status)
metrics.gauge(’slsTntRouterAlerts‘):set(routerinfo.alerts	nil and tfrouterinfo.alerts or 0)
if router_info.bucket nil then
metrics.gauge(’slsTntRouterUnreachableBuckets‘:set(routerinfo.bucket.unreachable)
metrics.gauge(‘slsTntRouterUnknownBuckets‘:set(router info.bucket.unknown)
if router_info.bucket ~= nil then
metrics . gauge ( ' slsTntRouterllnreachableBuckets '): set (routerinfo, bucket .unreachable)
metrics.gauge(’ slsTntRouterllnknownBuckets '): set(router_info. bucket.unknown)
metrics.gauge(‘slsTntRouterAvailableRoBuckets'):set(router_info.bucket.available_ro)
metrics.gauge(’slsTntRouterAvailableRwBuckets'):set(router_info.bucket.available_rw)
end
end
end
local res, storageinfo = pcall(vshard.storage.info)
if res then
if storage_info ~= nil then
metrics.gauge('slsTntStorageStatus'):set(storage_info.status)
metrics.gauge('slsTntStorageAlerts'):set(storage_info.alerts ~= nil and #storage_info.alerts or 0)
if storageinfo.bucket ~= nil then
metrics.gauge('slsTntStorageTotalBuckets'):set(storage_info.bucket.total)
metrics.gauge('slsTntStorageActiveBuckets'):set(storage_info.bucket.active)
metrics.gauge(’slsTntStorageGarbageBuckets'):set(storage_info.bucket.garbage)
metrics.gauge('slsTntStorageReceivingBuckets’):set(storage_info.bucket.receiving)
metrics.gauge('slsTntStorageSendingBuckets'):set(storage_info.bucket.sending)
end
■
local uuid = box.info.uuid
local repl = box.info.replication
if uuid ~= nil and repl ~= nil then
for i, r in ipairs(repl) do
if uuid ~= r.uuid then
local corr_uuid = r.uuid or "null"
local corr_upstream_status = r.upstream and r.upstream.status or "stopped"
local corr_downstream_status = r.downstream and r.downstream.status or "stopped"
metrics.gauge(’slsTntReplicationStatus_info'):set(l, {["direction"] = "uuid="..c
metrics,gauge(’slsTntReplicationStatus_info'):set(l, {["direction"] = "uuid=”..c
end
end
end
end
end
end
■_uuid..“, upstream", ["status"] = corr_upstream_status})
■_uuid..", downstream", ["status"] = corr_downstream_status})
)
end
return{
init_metrics = init_metrics
} ]]


--[[ else
return alternative
end
end
return alternative
end ]]

local function date_max(...)
    local dmax = 0
    for v in ipairs({...}) do
        if nvl(v, 0) > dmax then
            dmax = v
        end
    end
    return dmax
end

--[[ ) then ]]

--[[ local function date_msg_id_max(date_l, msg_id_lj date_2.
if (nvl(date_l, 0)
return date_l?
else
return date_2j
end
end
> nvl(date_2j 0)) or (nvl(date_lJ 1
msg_id_l
msg_id_2

id_2)
nvl(da
idl, 0) > nvlfmsg id 2, 0)) then ]]

--[[ function table.val_to_str ( v )
if "string" == type( v ) then
v = string.gsub( v, “\n"j "\\n” )
if string.match( string.gsub(v,”[A’\"]"
return ........... .. v .. ..
end
return "" .. string.gsub(Vj1‘ \\‘"
end
return "table" == type( v ) and table.tostring( v ) or tostring( v )
end
function table.key_to_str ( k )
if "string" == type( k ) and string.match( k, ”A[_%a][_%a%d]*$“ ) then
return k
end
return "[" .. table.val_to_str( k ) .. ”]"
end
function table.tostring( tbl )
if tbl == nil then
return nil
if tbl == nil then
return nil
end
local result, done = {}, {}
for k, v in ipairs( tbl ) d
table.insert( result, table.val_to_str( v ) )
done[ k ] = true
end
for
k
v in pairs( tbl ) do
not done[ k ] then
table.insert( result, table.key_to_str( k
end
table.val to
end
return "{" .. table.concat( result
end ]]

local function table_is_empty(tbl)
    for v in pairs(tbl) do
        if (type(v) == "table") then
            if table_is_empty(v) == false then
                return false
            end
        else
            return false
        end
    end
    return true
end

local function table_length(tbl)
    if tbl == nil then
        return nil
    end
    local length = 0
    for v in pairs(tbl) do
        length = length + 1
    end
    return length
end

local function table_complement(tl, t2)
    local res = {}
    for _,v2 in pairs(t2) do
        local found = false
        for _,vl in pairs(tl) do
            if (v2[1] == vl) then
                found = true
                break
            end
        end
        if not found then table.insert(res, v2) end
    end
    return res
end

local function tables_is_equal(tl, t2) -- work correctly only with array
    if tl == nil and t2 == nil then
        return true
    elseif tl == nil or t2 == nil then
        return false
    end
    if #tl ~= #t2 then
        return false
    end
    for i=1, #tl do
        if tl[i] ~= t2[i] then
            return false
        end
    end
    return true
end

local function table_contains(table, element)
    for _, value in pairs(table) do
        if value == element then
            return true
        end
    end
return false
end

local function check_type(v, t)
    if (isnumber(v) and t == "Long") or
        (type(v) == "string" and t == "String") or
        (type(v) == "boolean" and t == "Boolean")
    then
        return true
    end
    return false
end

local trace_mode = false
local func_for_trace = {}
local customer_id_for_trace = {}
local subscriber_id_for_trace = {}

local function trace(func, tbl, label)
    -- func     -- func name, string, mandatory
    -- tbl      -- structure for log, string/map/array of map, mandatory
                -- string could be logged only when customer_id_for_trace and
                -- when customer_id_for_trace or subscriber_id_for_trace is not nil, standalone map or map in array
                -- should contain customer_id or subscriber_id field
    -- label    -- mark of trace point, string

    if not trace_mode then
        return
    end

    local msg
    if  (func == nil) or
        (func == '') or
        (type(func) ~= 'string')
    then
        msg = "*** trace: Incorrect func format"
        log.info(msg)
        return msg
    end
    
    if  (tbl == nil) or
        (
            type(tbl) ~= 'string' and
            type(tbl) ~= 'table'
        ) or
        (    
            type(tbl) == 'table' and
            table_length(tbl) == 0
        )
    then
        msg = "*** trace: Incorrect tbl format"
        log.info(msg)
        return msg
    end

    if label ~= nil and type(label) ~= 'string' then
        msg = "*** trace: Incorrect label format"
        log.info(msg)
        return msg
    end

    local func_found = false
    if func_for_trace ~= nil then
        for _, f in pairs(func_for_trace) do
            if  (func == f) or
                (string.sub(func, 9) == string.sub(f, 5)) or -- for different prefixes: sls_tnt_ and sls_
                (string.sub(func, 5) == string.sub(f, 9))
            then
                func_found = true
                break
            end
        end
    end

    local customer_id_found = false
    if customer_id_for_trace ~= nil and type(tbl) ~= 'string' then
        if tbl.customer_id ~= nil then
            for _, i in pairs(customer_id_for_trace) do
                if tbl.customer_id == i then
                    customer_id_found = true
                    break
                end
            end
        else
            for _, v in ipairs(tbl) do
                for _, i in pairs(customer_id_for_trace) do
                    if v.customer_id == i then
                        customer_id_found = true
                        break
                    end
                end
                if customer_id_found == true then
                    break
                end
            end
        end
    end

    local subscriber_id_found = false
    if subscriber_id_for_trace ~= nil and type(tbl) ~= 'string' then
        if tbl.subscriber_id ~= nil then
            for _, i in pairs(subscriber_id_for_trace) do
                if tbl.subscriber_id == i then
                    subscriber_id_found = true
                    break
                end
            end
        else
            for _, v in ipairs(tbl) do
                for _, i in pairs(subscriber_id_for_trace) do
                    if v.subscriber_id == i then
                        subscriber_id_found = true
                        break
                    end
                end
                if subscriber_id_found == true then
                    break
                end
            end
        end
    end

    if  (
            (func_for_trace == nil) or
            (func_for_trace ~= nil and func_found)
        ) and
        (
            (customer_id_for_trace ~= nil and customer_id_found) or
            (subscriber_id_for_trace ~= nil and subscriber_id_found) or
            (customer_id_for_trace == nil and subscriber_id_for_trace == nil)
        )
    then
        msg = "*** trace for "..func
        if label ~= nil then
            msg = msg..", "..label
        end
        msg = msg..": "..json.encode(tbl)
        log.info(msg)
        return msg
    end
end

local function set_trace_on(
    func,    -- one func name or array of func names, nil -> all funcs
    clnt_id, -- one clnt_id or array of clntid's, nil -> all clnt_id’s
    subs_id  -- one subs_id or array of subs_id’s, nil -> all subsid’s
)
    local func_for_trace_tmp = {}
    if  (func == nil) or
        (func == '') or
        (
            type(func) == 'table' and 
            table_length(func) == 0
        )
    then
        func_for_trace_tmp = nil
    elseif type(func) == 'string' then
        func_for_trace_tmp = {}
        table.insert(func_for_trace_tmp, func)
    elseif type(func) == 'table' and #func == table_length(func) then
        func_for_trace_tmp = {}
        for _, f in ipairs(func) do
            if f ~= nil and f ~= '' and type(f) == 'string' then
                table.insert(func_for_trace_tmp, f)
            else
                return error("Incorrect func format")
            end
        end
    else
        return error("Incorrect func format")
    end

    local customer_id_for_trace_tmp = {}
    if  (clnt_id == nil) or 
        (
            type(clnt_id) == 'table' and
            table_length(clnt_id) == 0
        )
    then
        customer_id_for_trace_tmp = nil
    elseif isnumber(clnt_id) then
        customer_id_for_trace_tmp = {}
        table.insert(customer_id_for_trace_tmp, clnt_id)
    elseif type(clnt_id) == 'table' and #clnt_id == table_length(clnt_id) then
        customer_id_for_trace_tmp = {}
        for _, c in ipairs(clnt_id) do
            if c ~= nil and isnumber(c) then
                table.insert(customer_id_for_trace_tmp, c)
            else
                return error("Incorrect clnt_id format")
            end
        end
    else
        return error("Incorrect clnt_id format")
    end

    local subscriber_id_for_trace_tmp = {}
    if  (subs_id == nil) or
        (
            type(subs_id) == 'table' and
            table_length(subs_id) == 0
        )
    then
        subscriber_id_for_trace_tmp = nil
    elseif isnumber(subs_id) then
        subscriber_id_for_trace_tmp = {}
        table.insert(subscriber_id_for_trace_tmp, subs_id)
    elseif type(subs_id) == 'table' and #subs_id == table_length(subs_id) then
        subscriber_id_for_trace_tmp = {}
        for _, s in ipairs(subs_id) do
            if s ~= nil and isnumber(s) then
                table.insert(subscriber_id_for_trace_tmp, s)
            else
                return error("Incorrect subs_id format")
            end
        end
    else
        return error("Incorrect subs_id format")
    end

    func_for_trace = func_for_trace_tmp
    customer_id_for_trace = customer_id_for_trace_tmp
    subscriber_id_for_trace = subscriber_id_for_trace_tmp
    trace_mode = true

    return (
        nvl2(func_for_trace, "nil", json.encode(func_for_trace))..", "..
        nvl2(customer_id_for_trace, "nil", json.encode(customer_id_for_trace))..", "..
        nvl2(subscriber_id_for_trace, "nil", json.encode(subscriber_id_for_trace))
    )
end

local function set_trace_off()
    trace_mode = false
    return true
end
