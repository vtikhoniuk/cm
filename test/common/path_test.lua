local helper = require('test.helper')
local log = require('log')
local json = require('json')
local fiber = require('fiber')
local test_utils = require('test.test_utils')
local utils = require('app.tools.utils')
local t2s = require('app.tools.t2s')

local t = require('luatest')
local g = t.group('path')

g.before_each(function()
    test_utils.truncate_all()
end)

g.test_path_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {{orig_set = {10111}, flip_set = {20111}}},
            20111
        }
    )
    t.assert_equals(r, {{own = 10111, path = {1}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_path_2 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {{orig_set = {10111}, flip_set = {20111}, cancel = true}},
            20111
        }
    )
    t.assert_equals(r, {})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_path_3 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {20111}, flip_set = {30111}}
            },
            30111
        }
    )
    t.assert_equals(r, {{own=10111,path={1,2}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_path_4 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20211}},
                {orig_set = {20111,20211}, flip_set = {30111}}
            },
            30111
        }
    )
    t.assert_equals(r, {{own=10111,path={1,3}},{own=10211,path={2,3}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_path_5 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20211}, cancel = true},
                {orig_set = {20111,20211}, flip_set = {30111}}
            },
            30111
        }
    )
    t.assert_equals(r, {})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_path_6 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20211}, cancel = true},
                {orig_set = {10311}, flip_set = {20211}},
                {orig_set = {20111,20211}, flip_set = {30111}}
            },
            30111
        }
    )
    t.assert_equals(r, {{own=10111,path={1,4}},{own=10311,path={3,4}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_path_7 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20211}, cancel = true},
                {orig_set = {20111,20211}, flip_set = {30111}},
                {orig_set = {10411}, flip_set = {30111}},
            },
            30111
        }
    )
    t.assert_equals(r, {{own=10411,path={4}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_path_8 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20211}, cancel = true},
                {orig_set = {10311}, flip_set = {20211}},
                {orig_set = {20111,20211}, flip_set = {30111}},
                {orig_set = {10411}, flip_set = {30111}},
            },
            30111
        }
    )
    t.assert_equals(r, {{own=10111,path={1,4}},{own=10311,path={3,4}},{own=10411,path={5}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_path_9 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_path_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111,40311}},
                {orig_set = {10211}, flip_set = {20211}, cancel = true},
                {orig_set = {10311}, flip_set = {20211}},
                {orig_set = {20111,20211}, flip_set = {30111}},
                {orig_set = {10411}, flip_set = {30111}},
                {orig_set = {30111}, flip_set = {40111}},
                {orig_set = {40211}, flip_set = {10211}},
                {orig_set = {10111}, flip_set = {40411}},
            },
            30111
        }
    )
    t.assert_equals(r, {{own=10111,path={1,4}},{own=10311,path={3,4}},{own=10411,path={5}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end