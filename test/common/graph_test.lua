local helper = require('test.helper')
local log = require('log')
local json = require('json')
local fiber = require('fiber')
local test_utils = require('test.test_utils')
local utils = require('app.tools.utils')
local t2s = require('app.tools.t2s')

local t = require('luatest')
local g = t.group('graph')

g.before_each(function()
    test_utils.truncate_all()
end)

g.test_resolve_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
        "}"..
    ")")
    t.assert_equals(r, {{1,2},{2,1}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_2 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211, 20311}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {20311}}, "..
        "}"..
    ")")
    t.assert_equals(r, {})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_3 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211, 20311}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111,20211}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {20311}}, "..
        "}"..
    ")")
    t.assert_equals(r, {{1,3},{3,1}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_4 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20111}}, "..
        "}"..
    ")")
    t.assert_equals(r, {{1},{2}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_5 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10111}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
        "}"..
    ")")
    t.assert_equals(r, {{1,3},{3,1}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_6 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111,20211}}, "..
            "{orig_set = {10211}, flip_set = {20211,20111}}, "..
        "}"..
    ")")
    t.assert_equals(r, {{1},{2}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_7 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
        "}"..
    ")")
    t.assert_equals(r, {{1,2,3},{1,3,2},{3,1,2}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_8 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10111,10211}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111,20211}}, "..
        "}"..
    ")")
    t.assert_equals(r, {{2,3}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_9 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111,30211}}, "..
            "{orig_set = {10211}, flip_set = {30311}}, "..
            "{orig_set = {30311,30211}, flip_set = {20211}}, "..
        "}"..
    ")")
    t.assert_equals(r, {{1,2,3,4},{1,3,2,4},{3,1,2,4}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_resolve_10 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10112, 10211}, "..
        "{10111, 20211}, "..
        "{"..
            "{orig_set = {10111,10211}, flip_set = {20211}}, "..
        "}"..
    ")")
    log.info('**** result = '..t2s.t2s(r, 2))
    t.assert_equals(r, {{1}})

    --t.fail()
end

g.test_resolve_12 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10112,10211,10311}, "..
        "{20111,20211,20311}, "..
        "{"..
            "{orig_set = {10211}, flip_set = {30211}}, "..
            "{orig_set = {30211,30411}, flip_set = {20211,20311}}, "..
            "{orig_set = {10311}, flip_set = {30311}}, "..
            "{orig_set = {30311}, flip_set = {20311}}, "..
            "{orig_set = {30211}, flip_set = {30111}}, "..
            "{orig_set = {10311}, flip_set = {30411}}, "..
            "{orig_set = {30311,30111}, flip_set = {20111}}, "..
            "{orig_set = {10111}, flip_set = {20111}} "..
        "} "..
    ")")

    log.info('**** result = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_resolve_13 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111,10211}, "..
        "{20111,20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111,30211}, flip_set = {20111,20211}}, "..
            "{orig_set = {10211}, flip_set = {30211}}, "..
        "} "..
    ")")

    log.info('**** result = '..t2s.t2s(r, 2))
    t.assert_equals(r, {{1,3,2},{3,1,2}})

    --t.fail()
end

g.test_resolve_13_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10111,10211}, "..
        "{20111,20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30211,30111}, flip_set = {20111,20211}}, "..
            "{orig_set = {10211}, flip_set = {30211}}, "..
        "} "..
    ")")

    log.info('**** result = '..t2s.t2s(r, 2))
    t.assert_equals(r, {{1,3,2},{3,1,2}})

    --t.fail()
end

g.test_resolve_14 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10112}, "..
        "{20111}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
        "} "..
    ")")

    log.info('**** result = '..t2s.t2s(r, 2))
    t.assert_equals(r, {{1}})

    --t.fail()
end

g.test_resolve_15 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{10112}, "..
        "{20111}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111}}, "..
        "} "..
    ")")

    log.info('**** result = '..t2s.t2s(r, 2))
    t.assert_equals(r, {{1,2}})

    --t.fail()
end

g.test_resolve_16 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{20111}, "..
        "{21411}, "..
        "{"..
            "{orig_set = {20111,22711}, flip_set = {21411}}, "..
        "} "..
    ")")

    log.info('**** result = '..t2s.t2s(r, 2))
    t.assert_equals(r, {})

    --t.fail()
end

g.test_resolve_16_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return resolve_graph("..
        "{20111}, "..
        "{21411}, "..
        "{"..
            "{orig_set = {22711,20111}, flip_set = {21411}}, "..
        "} "..
    ")")

    log.info('**** result = '..t2s.t2s(r, 2))
    t.assert_equals(r, {})

    --t.fail()
end