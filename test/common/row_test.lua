local helper = require('test.helper')
local log = require('log')
local json = require('json')
local fiber = require('fiber')
local test_utils = require('test.test_utils')
local utils = require('app.tools.utils')
local t2s = require('app.tools.t2s')

local t = require('luatest')
local g = t.group('row')

g.before_each(function()
    test_utils.truncate_all()
end)

g.test_row_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_row_set_beginning_from_card",
        {
            {{orig_set = {10111}, flip_set = {20111}}},
            20111
        }
    )
    t.assert_equals(r, {{card_set = {10111,20111}, change_set = {1}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_row_2 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_row_set_beginning_from_card",
        {
            {{orig_set = {10111}, flip_set = {20111}}},
            10111
        }
    )
    t.assert_equals(r, {{card_set = {10111}, change_set = {}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_row_3 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_row_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20211}}
            },
            20111
        }
    )
    t.assert_equals(r, {{card_set = {10111,20111}, change_set = {1}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_row_4 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_row_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20111}}
            },
            20111
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,20111}, change_set = {1}},
        {card_set = {10211,20111}, change_set = {2}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_row_5 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_row_set_beginning_from_card",
        {
            {
                {orig_set = {10111,10211}, flip_set = {20111}}
            },
            20111
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,10211,20111}, change_set = {1}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_row_6 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "get_row_set_beginning_from_card",
        {
            {
                {orig_set = {10111}, flip_set = {20111,20211}}
            },
            20111
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,20111}, change_set = {1}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end