local helper = require('test.helper')
local log = require('log')
local json = require('json')
local fiber = require('fiber')
local test_utils = require('test.test_utils')
local utils = require('app.tools.utils')
local auth_config = require('app.auth.config')

local t = require('luatest')
local g = t.group('auth')

g.before_each(function()
    test_utils.truncate_all()
end)

g.test_reg = function()
    local router = helper.cluster.main_server
    local response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.user_status()
    --t.fail()
end

g.test_reg_with_simple_pass = function()
    local router = helper.cluster.main_server
    local response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'aaaaaaaa'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    --t.fail()
end

g.test_reg_without_pass = function()
    local router = helper.cluster.main_server
    local response = router:http_request('post', '/signup', {json = {email = 'A@b.com'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    --t.fail()
end

g.test_reg_duplicate = function()
    local router = helper.cluster.main_server
    local response
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12_'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end

g.test_reg_anonymous = function()
    local router = helper.cluster.main_server
    local response = router:http_request('post', '/signup', {json = {}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    --t.fail()
end

--[[ g.test_confirm_email = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local code = response.json.code
    response = router:http_request('post', '/confirm', {json = {email = 'A@b.com', code = code}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    --t.fail()
end ]]

--[[ g.test_confirm_incorrect = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    response = router:http_request('post', '/confirm', {json = {email = 'A@b.com', code = '1111'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end ]]

--[[ g.test_confirm_incorrect2 = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local code = response.body
    response = router:http_request('post', '/confirm', {json = {email = 'A1@b.com', code = code}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end ]]

g.test_restore = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    local code = response.json.code
    --[[ response = router:http_request('post', '/confirm', {json = {email = 'A@b.com', code = code}})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200) ]]
    response = router:http_request('post', '/restore', {json = {email = 'A@b.com'}, raise = false})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    local token = response.json
    response = router:http_request('post', '/complete', {json = {email = 'A@b.com', token = token, password = 'Abcde12_'}, raise = false})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    --t.fail()    
end

g.test_restore_incorrect = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local code = response.json.code
    --[[ response = router:http_request('post', '/confirm', {json = {email = 'A@b.com', code = code}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status() ]]
    response = router:http_request('post', '/restore', {json = {email = 'A1@b.com'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end

g.test_restore_incorrect2 = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local code = response.json.code
    --[[ response = router:http_request('post', '/confirm', {json = {email = 'A@b.com', code = code}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status() ]]
    response = router:http_request('post', '/restore', {json = {email = 'A@b.com'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local token = response.json
    response = router:http_request('post', '/complete', {json = {email = 'A1@b.com', token = token, password = 'Abcde12_'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end

g.test_restore_incorrect3 = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local code = response.json.code
    --[[ response = router:http_request('post', '/confirm', {json = {email = 'A@b.com', code = code}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status() ]]
    response = router:http_request('post', '/restore', {json = {email = 'A@b.com'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local token = response.json
    response = router:http_request('post', '/complete', {json = {email = 'A@b.com', token = '111111', password = 'Abcde12_'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end

g.test_restore_incorrect4 = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local
     code = response.json.code
    --[[ response = router:http_request('post', '/confirm', {json = {email = 'A@b.com', code = code}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status() ]]
    response = router:http_request('post', '/restore', {json = {email = 'A@b.com'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    local token = response.json
    response = router:http_request('post', '/complete', {json = {email = 'A@b.com', token = token, password = '11111111'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end

g.test_login = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    response = router:http_request('post', '/signin', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    --t.fail()
end

g.test_login_incorrect = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    response = router:http_request('post', '/signin', {json = {email = 'A1@b.com', password = 'Abcde12*'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end

g.test_login_incorrect2 = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    response = router:http_request('post', '/signin', {json = {email = 'A@b.com', password = 'Abcde12_'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end

g.test_login_incorrect3 = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    response = router:http_request('post', '/signin', {json = {email = 'A@b.com'}, raise = false})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    --t.fail()
end

g.test_logout = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}, raise = false})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    response = router:http_request('post', '/signin', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    local session = response.json.session
    log.info('***** session = '..json.encode(session))
    response = router:http_request('post', '/signout', {json = {session = session}, raise = false})
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    --t.fail()
end

g.test_logout_incorrect = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    response = router:http_request('post', '/signin', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    t.assert_equals(response.status, 200)
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    response = router:http_request('post', '/signout', {json = {session = 'eyZza'}, raise = false})
    t.assert_equals(response.status, 500)
    test_utils.user_status()
    log.info('***** response = '..json.encode(response))
    --t.fail()
end

g.test_check = function()
    local router = helper.cluster.main_server
    local response 

    test_utils.set_timeframes(1000*3, 1000)
    
    response = router:http_request('post', '/signup', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    test_utils.user_status()
    log.info('***** response for /signup = '..json.encode(response))
    t.assert_equals(response.status, 200)

    --[[ test_utils.set_timeframes(1000*3, 1000)
    --log.info('&&&& SESSION_LIFETIME'..auth_config.SESSION_LIFETIME)

    response = router:http_request('post', '/signin', {json = {email = 'A@b.com', password = 'Abcde12*'}})
    test_utils.user_status()
    log.info('***** response for /signin = '..json.encode(response))
    t.assert_equals(response.status, 200) ]]

    local session_1 = response.json.session
    log.info('#### session_1 = '..json.encode(session_1))
    t.assert(session_1)

    fiber.sleep(2)

    response = router:http_request('post', '/check', {json = {session = session_1}, raise = false})
    test_utils.user_status()
    log.info('***** response for /check 1 = '..json.encode(response))
    t.assert_equals(response.status, 200)

    local session_2 = response.json
    log.info('#### session_2 = '..json.encode(session_2))
    t.assert(session_2)
    t.assert_not_equals(session_1, session_2)

    fiber.sleep(4)

    response = router:http_request('post', '/check', {json = {session = session_2}, raise = false})
    test_utils.user_status()
    log.info('***** response for /check 2 = '..json.encode(response))
    t.assert_equals(response.status, 500)

    --t.fail()
end

g.test_check_anonymous = function()
    local router = helper.cluster.main_server
    local response 
    response = router:http_request('post', '/signup', {json = {}, raise = false})
    test_utils.user_status()
    log.info('***** response for /signup = '..json.encode(response))
    t.assert_equals(response.status, 200)

    test_utils.set_timeframes(1000*3, 1000)
    --log.info('&&&& SESSION_LIFETIME'..auth_config.SESSION_LIFETIME)

    local user_uuid = response.json.user_uuid
    log.info('**** user_uuid = '..tostring(user_uuid))
    t.assert(user_uuid)

    response = router:http_request('post', '/signin', {json = {user_uuid = user_uuid}, raise = false})
    test_utils.user_status()
    log.info('***** response for /signin = '..json.encode(response))
    t.assert_equals(response.status, 200)

    local session_1 = response.json.session
    log.info('#### session_1 = '..json.encode(session_1))
    t.assert(session_1)

    fiber.sleep(2)

    response = router:http_request('post', '/check', {json = {session = session_1}, raise = false})
    test_utils.user_status()
    log.info('***** response for /check 1 = '..json.encode(response))
    t.assert_equals(response.status, 200)

    local session_2 = response.json
    log.info('#### session_2 = '..json.encode(session_2))
    t.assert(session_2)
    t.assert_not_equals(session_1, session_2)

    fiber.sleep(4)

    response = router:http_request('post', '/check', {json = {session = session_2}, raise = false})
    test_utils.user_status()
    log.info('***** response for /check 2 = '..json.encode(response))
    t.assert_equals(response.status, 500)

    --t.fail()
end