local helper = require('test.helper')
local log = require('log')
local json = require('json')
local fiber = require('fiber')
local test_utils = require('test.test_utils')
local utils = require('app.tools.utils')
local t2s = require('app.tools.t2s')

local t = require('luatest')
local g = t.group('level')

g.before_each(function()
    test_utils.truncate_all()
end)

g.test_create_level = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return add_initial_suits()")
    t.assert(r)
    --test_utils.suit_status()

    r = storage1.net_box:eval("return create_level(5, 0, {10112})")
    t.assert(r)
    --test_utils.suit_status()
    log.info('**** level = '..t2s.t2s(r, 3))

    --t.fail()
end

g.test_create_fi_121 = function()
    local storage1 = helper.cluster:server('s1-1')

    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {30211}}, "..
            "{orig_set = {30211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1}}, "..
            "{own = 10211, col = 20211, path = {2,3}}, "..
        "}, "..
        "1"..
    ")")
    t.assert(r)
    
    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_121_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {30211}}, "..
            "{orig_set = {30211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1}}, "..
            "{own = 10211, col = 20211, path = {2,3}}, "..
        "}, "..
        "1, "..
        "1, "..
        "4"..
    ")")
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})
    
    --t.fail()
end

g.test_create_fi_121_2 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {30211}}, "..
            "{orig_set = {30211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1}}, "..
            "{own = 10211, col = 20211, path = {2,3}}, "..
        "}, "..
        "1, "..
        "1, "..
        "2"..
    ")")
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})
    --t.fail()
end

g.test_create_fi_121_3 = function()
    local storage1 = helper.cluster:server('s1-1')

    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {30211}}, "..
            "{orig_set = {30211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1}}, "..
            "{own = 10211, col = 20211, path = {2,3}}, "..
        "}, "..
        "2"..
    ")")
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_211 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1,2}}, "..
            "{own = 10211, col = 20211, path = {3}}, "..
        "}, "..
        "2"..
    ")")
    log.info('**** result = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_211_ = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return add_initial_suits()")
    t.assert(r)

    r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1,2}}, "..
            "{own = 10211, col = 20211, path = {3}}, "..
        "}, "..
        "2, "..
        "{2,1}, "..
        "{4,4} "..
    ")")
    log.info('**** result = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_211__ = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return add_initial_suits()")
    t.assert(r)

    r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1,2}}, "..
            "{own = 10211, col = 20211, path = {3}}, "..
        "}, "..
        "2, "..
        "{1,2}, "..
        "{4,4} "..
    ")")
    log.info('**** result = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_211_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1,2}}, "..
            "{own = 10211, col = 20211, path = {3}}, "..
        "}, "..
        "1, "..
        "3, "..
        "1"..
    ")")
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_211_2 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1,2}}, "..
            "{own = 10211, col = 20211, path = {3}}, "..
        "}, "..
        "1, "..
        "2, "..
        "4"..
    ")")
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})
    --t.fail()
end

g.test_create_fi_211_3 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        "{"..
            "{orig_set = {10111}, flip_set = {30111}}, "..
            "{orig_set = {30111}, flip_set = {20111}}, "..
            "{orig_set = {10211}, flip_set = {20211}}, "..
            "{orig_set = {10211}, flip_set = {30411}}, "..
        "}, "..
        "{"..
            "{own = 10111, col = 20111, path = {1,2}}, "..
            "{own = 10211, col = 20211, path = {3}}, "..
        "}, "..
        "1, "..
        "1, "..
        "4"..
    ")")
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        "{10111, 10211}, "..
        "{20111, 20211}, "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_11 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10512,21611,22011,22311}, "..
        "{21011,23011,10411,20411}, "..
        "{"..
            "{orig_set = {22011}, flip_set = {22211}}, "..
            "{orig_set = {22211}, flip_set = {23011}}, "..
            "{orig_set = {21611}, flip_set = {10411}}, "..
            "{orig_set = {22311}, flip_set = {20411}}, "..
            "{orig_set = {10511}, flip_set = {22111}}, "..
            "{orig_set = {22111}, flip_set = {21011}}, "..
            "{orig_set = {22211}, flip_set = {20611}}, "..
            "{orig_set = {22311}, flip_set = {21111}}, "..
            "{orig_set = {10511}, flip_set = {21211}}, "..
        "}, "..
        "{"..
            "{own = 22011, col = 23011, path = {1,2}}, "..
            "{own = 21611, col = 10411, path = {3}}, "..
            "{own = 22311, col = 20411, path = {4}}, "..
            "{own = 10511, col = 21011, path = {5,6}}, "..
        "}, "..
        "2, "..
        "{6,4}, "..
        "{7,7}, "..
        "{4}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_12 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{22312,21711,21011,10311}, "..
        "{20811,22611,10211,20911}, "..
        "{"..
            "{orig_set = {21711}, flip_set = {21511}}, "..
            "{orig_set = {21511}, flip_set = {10211}}, "..
            "{orig_set = {21011}, flip_set = {20811}}, "..
            "{orig_set = {10311}, flip_set = {22611}}, "..
            "{orig_set = {22311}, flip_set = {21211}}, "..
            "{orig_set = {21211}, flip_set = {20911}}, "..
            "{orig_set = {21711}, flip_set = {20411}}, "..
            "{orig_set = {21011}, flip_set = {22111}}, "..
            "{orig_set = {22111}, flip_set = {22711}}, "..
        "}, "..
        "{"..
            "{own = 21711, col = 10211, path = {1,2}}, "..
            "{own = 21011, col = 20811, path = {3}}, "..
            "{own = 10311, col = 22611, path = {4}}, "..
            "{own = 22311, col = 20911, path = {5,6}}, "..
        "}, "..
        "2, "..
        "{5,6}, "..
        "{9,7}, "..
        "{1,5}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_12_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{22412,20111,22211,20611}, "..
        "{22011,22611,21811,20211}, "..
        "{"..
            "{orig_set = {20611}, flip_set = {22611}}, "..
            "{orig_set = {20111}, flip_set = {23211}}, "..
            "{orig_set = {23211}, flip_set = {22011}}, "..
            "{orig_set = {22211}, flip_set = {21311}}, "..
            "{orig_set = {21311}, flip_set = {21811}}, "..
            "{orig_set = {22411}, flip_set = {20211}}, "..
            "{orig_set = {23211}, flip_set = {21711}}, "..
            "{orig_set = {22211}, flip_set = {21411}}, "..
            "{orig_set = {21411}, flip_set = {10511}}, "..
        "}, "..
        "{"..
            "{own = 20611, col = 22611, path = {1}}, "..
            "{own = 20111, col = 22011, path = {2,3}}, "..
            "{own = 22211, col = 21811, path = {4,5}}, "..
            "{own = 22411, col = 20211, path = {6}}, "..
        "}, "..
        "2, "..
        "{1,6}, "..
        "{8,9}, "..
        "{6}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_13 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10412,22811,21411,23111,22211}, "..
        "{10511,10711,22111,10111,11011}, "..
        "{"..
            "{orig_set = {22811}, flip_set = {22011}}, "..
            "{orig_set = {22011}, flip_set = {10711}}, "..
            "{orig_set = {22211}, flip_set = {11011}}, "..
            "{orig_set = {23111}, flip_set = {23011}}, "..
            "{orig_set = {23011}, flip_set = {22111}}, "..
            "{orig_set = {21411}, flip_set = {10111}}, "..
            "{orig_set = {22011}, flip_set = {22911}}, "..
            "{orig_set = {23111}, flip_set = {22611}}, "..
            "{orig_set = {23011}, flip_set = {10511}}, "..
            "{orig_set = {10412}, flip_set = {10511}} "..
        "}, "..
        "{"..
            "{own = 22811, col = 10711, path = {1,2}}, "..
            "{own = 22211, col = 11011, path = {3}}, "..
            "{own = 23111, col = 22111, path = {4,5}}, "..
            "{own = 21411, col = 10111, path = {6}}, "..
            "{own = 10412, col = 10511, path = {10}}, "..
            "{own = 23111, col = 10511, path = {4,9}}, "..
        "}, "..
        "2, "..
        "{2,9}, "..
        "{8,7}, "..
        "{2}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_13_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10412,22811,23111}, "..
        "{10511,10711,22111}, "..
        "{"..
            "{orig_set = {22811}, flip_set = {22011}}, "..
            "{orig_set = {22011}, flip_set = {10711}}, "..
            "{orig_set = {23111}, flip_set = {23011}}, "..
            "{orig_set = {23011}, flip_set = {22111}}, "..
            "{orig_set = {22011}, flip_set = {22911}}, "..
            "{orig_set = {23111}, flip_set = {22611}}, "..
            "{orig_set = {23011}, flip_set = {10511}}, "..
            "{orig_set = {10411}, flip_set = {10511}} "..
        "}, "..
        "{"..
            "{own = 22811, col = 10711, path = {1,2}}, "..
            "{own = 23111, col = 22111, path = {3,4}}, "..
            "{own = 10412, col = 10511, path = {8}}, "..
            "{own = 23111, col = 10511, path = {3,7}}, "..
        "}, "..
        "2, "..
        "{2,7}, "..
        "{6,5}, "..
        "{2}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_13_2 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{22512,22011,21111,10611,10511}, "..
        "{22311,10111,21311,10811,22911}, "..
        "{"..
            "{orig_set = {22011}, flip_set = {10111}}, "..
            "{orig_set = {10511}, flip_set = {10811}}, "..
            "{orig_set = {10611}, flip_set = {21011}}, "..
            "{orig_set = {21011}, flip_set = {22911}}, "..
            "{orig_set = {22511}, flip_set = {21311}}, "..
            "{orig_set = {21111}, flip_set = {22311}}, "..
            "{orig_set = {10511}, flip_set = {21711}}, "..
            "{orig_set = {10611}, flip_set = {21911}}, "..
            "{orig_set = {22511}, flip_set = {21611}}"..
        "}, "..
        "{"..
            "{own = 22011, col = 10111, path = {1}}, "..
            "{own = 10511, col = 10811, path = {2}}, "..
            "{own = 10611, col = 22911, path = {3,4}}, "..
            "{own = 22511, col = 21311, path = {5}}, "..
            "{own = 21111, col = 22311, path = {6}}"..
        "}, "..
        "2, "..
        "{1,5}, "..
        "{7,7}, "..
        "{3}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_23 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{10312,22911,21311,20511,22311}, "..
        "{10711,22611,20611,21211,23211}, "..
        "{"..
            "{orig_set = {10311}, flip_set = {22211}}, "..
            "{orig_set = {22211}, flip_set = {23211}}, "..
            "{orig_set = {20511}, flip_set = {10611}}, "..
            "{orig_set = {10611}, flip_set = {20611}}, "..
            "{orig_set = {22311}, flip_set = {22111}}, "..
            "{orig_set = {22111}, flip_set = {10711}}, "..
            "{orig_set = {21311}, flip_set = {21411}}, "..
            "{orig_set = {21411}, flip_set = {21211}}, "..
            "{orig_set = {22911}, flip_set = {22611}}, "..
            "{orig_set = {10311}, flip_set = {20311}}, "..
            "{orig_set = {22211}, flip_set = {20411}}, "..
            "{orig_set = {10611}, flip_set = {20911}}, "..
            "{orig_set = {22311}, flip_set = {23111}}, "..
            "{orig_set = {21311}, flip_set = {20211}}"..        
        "}, "..
        "{"..
            "{own = 10311, col = 23211, path = {1,2}}, "..
            "{own = 20511, col = 20611, path = {3,4}}, "..
            "{own = 22311, col = 10711, path = {5,6}}, "..
            "{own = 21311, col = 21211, path = {7,8}}, "..
            "{own = 22911, col = 22611, path = {9}}"..
        "}, "..
        "4, "..
        "{3,1,10,14}, "..
        "{11,13,14,5}, "..
        "{9}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_22 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{21512,10811,10911,10411,10511}, "..
        "{10311,23211,22511,20211,21811}, "..
        "{"..
            "{orig_set = {10911}, flip_set = {10111}}, "..
            "{orig_set = {10111}, flip_set = {10311}}, "..
            "{orig_set = {10411}, flip_set = {21111}}, "..
            "{orig_set = {21111}, flip_set = {20911}}, "..
            "{orig_set = {20911}, flip_set = {22511}}, "..
            "{orig_set = {10511}, flip_set = {21411}}, "..
            "{orig_set = {21411}, flip_set = {20211}}, "..
            "{orig_set = {21511}, flip_set = {21811}}, "..
            "{orig_set = {10811}, flip_set = {23211}}, "..
            "{orig_set = {10111}, flip_set = {22411}}, "..
            "{orig_set = {21111}, flip_set = {10711}}, "..
            "{orig_set = {20911}, flip_set = {10211}}, "..
            "{orig_set = {10211}, flip_set = {22411}}, "..
            "{orig_set = {10511}, flip_set = {22311}}"..
        "}, "..
        "{"..
            "{own = 10911, col = 10311, path = {1,2}}, "..
            "{own = 10411, col = 22511, path = {3,4,5}}, "..
            "{own = 10511, col = 20211, path = {6,7}}, "..
            "{own = 21511, col = 21811, path = {8}}, "..
            "{own = 10811, col = 23211, path = {9}}"..
        "}, "..
        "4, "..
        "{8}, "..
        "{10}, "..
        "{9}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_21 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{22612,22011,20211,10211,20511}, "..
        "{22511,10511,22211,22911,22411}, "..
        "{"..
            "{orig_set = {22011}, flip_set = {21911}}, "..
            "{orig_set = {21911}, flip_set = {22511}}, "..
            "{orig_set = {22611}, flip_set = {22911}}, "..
            "{orig_set = {10211}, flip_set = {10811}}, "..
            "{orig_set = {10811}, flip_set = {22711}}, "..
            "{orig_set = {22711}, flip_set = {22411}}, "..
            "{orig_set = {20511}, flip_set = {10511}}, "..
            "{orig_set = {20211}, flip_set = {10911}}, "..
            "{orig_set = {10911}, flip_set = {22211}}, "..
            "{orig_set = {20511}, flip_set = {21811}}, "..
            "{orig_set = {21811}, flip_set = {20711}}, "..
            "{orig_set = {20711}, flip_set = {21511}}, "..
            "{orig_set = {10911}, flip_set = {10611}}, "..
            "{orig_set = {10611}, flip_set = {22111}}, "..       
        "}, "..
        "{"..
            "{own = 22011, col = 22511, path = {1,2}}, "..
            "{own = 22611, col = 22911, path = {3}}, "..
            "{own = 10211, col = 22411, path = {4,5,6}}, "..
            "{own = 20511, col = 10511, path = {7}}, "..
            "{own = 20211, col = 22211, path = {8,9}}"..
        "}, "..
        "4, "..
        "{2,9,3,7}, "..
        "{4,5,11,14}, "..
        "{2,7}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    log.info('**** resolve_graph = '..t2s.t2s(r, 2))
    t.assert_not_equals(r, {})

    --t.fail()
end

g.test_create_fi_lvl_24 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return create_fake_incoming_branches("..
        "{20412,21711,20911,10911,10311}, "..
        "{21111,11011,22511,23011,21411}, "..
        "{"..
            "{orig_set = {20411}, flip_set = {21111}}, "..
            "{orig_set = {21711}, flip_set = {21211}}, "..
            "{orig_set = {21211}, flip_set = {11011}}, "..
            "{orig_set = {10311}, flip_set = {10211}}, "..
            "{orig_set = {10211}, flip_set = {20511}}, "..
            "{orig_set = {20511}, flip_set = {23011}}, "..
            "{orig_set = {20911}, flip_set = {20111}}, "..
            "{orig_set = {20111}, flip_set = {21411}}, "..
            "{orig_set = {10911}, flip_set = {22511}}, "..
            "{orig_set = {20411}, flip_set = {22711}}, "..
            "{orig_set = {10311}, flip_set = {22811}}, "..
            "{orig_set = {10211}, flip_set = {22411}}, "..
            "{orig_set = {20111}, flip_set = {10811}}, "..
            "{orig_set = {10911}, flip_set = {21611}}, "..
        "}, "..
        "{"..
            "{own = 20411, col = 21111, path = {1}}, "..
            "{own = 21711, col = 11011, path = {2,3}}, "..
            "{own = 10311, col = 23011, path = {4,5,6}}, "..
            "{own = 20911, col = 21411, path = {7,8}}, "..
            "{own = 10911, col = 22511, path = {9}}"..
        "}, "..
        "4, "..
        "{8,5,3}, "..
        "{10,13,11}, "..
        "{9,4,3}"..
    ")")
    log.info('**** level = '..t2s.t2s(r, 3))
    t.assert(r)

    r = storage1.net_box:eval("return resolve_graph("..
        t2s.t2s(r.own_set)..", "..
        t2s.t2s(r.col_set)..", "..
        t2s.t2s(r.offer_set)..
    ")")
    for i=1, 9 do
        log.info('**** result '..i..'  = '..t2s.t2s(r[i], 1))
    end
    log.info('**** result output is stopped')
    t.assert_not_equals(r, {})

    --t.fail()
end