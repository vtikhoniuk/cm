local helper = require('test.helper')
local log = require('log')
local json = require('json')

local t = require('luatest')
local g = t.group('common')

g.test_simple = function()
    local router = helper.cluster.main_server
    t.assert_equals(router.net_box:eval('return box.cfg.memtx_dir'), router.workdir)
end

g.test_http = function()
    local router = helper.cluster.main_server
    local response = router:http_request('post', '/admin/api', {json = {query = '{ cluster { self { alias } } }'}})
    t.assert_equals(response.json, {data = { cluster = { self = { alias = 'r1' } } }})
end

g.test_metrics = function()
    local router = helper.cluster.main_server
    local response = router:http_request('get', '/metrics')
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
end

g.test_remove = function()
    local storage1 = helper.cluster:server('s1-1')
    local tbl = {1,2,3}
    for i,j in ipairs(tbl) do
        table.remove(tbl, i)
        log.info('**** tbl = '..json.encode(tbl))
    end

    t.assert_equals(tbl,{2})

    --t.fail()
end

