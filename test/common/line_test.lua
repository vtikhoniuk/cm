local helper = require('test.helper')
local log = require('log')
local json = require('json')
local fiber = require('fiber')
local test_utils = require('test.test_utils')
local utils = require('app.tools.utils')
local t2s = require('app.tools.t2s')

local t = require('luatest')
local g = t.group('line')

g.before_each(function()
    test_utils.truncate_all()
end)

g.test_1 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "build_line_set",
        {
            {{orig_set = {10111}, flip_set = {20111}}}
        }
    )
    t.assert_equals(r, {{card_set = {10111,20111}, change_set = {1}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_2 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "build_line_set",
        {
            {
                {orig_set = {10111}, flip_set = {30111}},
                {orig_set = {30111}, flip_set = {20111}}
            }
        }
    )
    t.assert_equals(r, {{card_set = {10111,20111,30111}, change_set = {1,2}}})
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_3 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "build_line_set",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20211}}
            }
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,20111}, change_set = {1}},
        {card_set = {10211,20211}, change_set = {2}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_4 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "build_line_set",
        {
            {
                {orig_set = {10111}, flip_set = {20111}},
                {orig_set = {10211}, flip_set = {20111}}
            }
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,20111}, change_set = {1}},
        {card_set = {10211,20111}, change_set = {2}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_5 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "build_line_set",
        {
            {
                {orig_set = {10111}, flip_set = {30111}},
                {orig_set = {10211}, flip_set = {30111}},
                {orig_set = {30111}, flip_set = {20111}}
            }
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,30111,20111}, change_set = {1,3}},
        {card_set = {10211,30111,20111}, change_set = {2,3}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_6 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "build_line_set",
        {
            {
                {orig_set = {30111}, flip_set = {20111}},
                {orig_set = {10111}, flip_set = {30111}},
                {orig_set = {10211}, flip_set = {30111}}
            }
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,30111,20111}, change_set = {1,2}},
        {card_set = {10211,30111,20111}, change_set = {1,3}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_7 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "build_line_set",
        {
            {
                {orig_set = {10111,10211}, flip_set = {20111}}
            }
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,10211,20111}, change_set = {1}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end

g.test_8 = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:call(
        "build_line_set",
        {
            {
                {orig_set = {10111}, flip_set = {20111,20211}}
            }
        }
    )
    t.assert_equals(r, {
        {card_set = {10111,20111,20211}, change_set = {1}}
    })
    log.info('**** result = '..t2s.t2s(r, 2))

    --t.fail()
end