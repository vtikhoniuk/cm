local helper = require('test.helper')
local log = require('log')
local json = require('json')
local fiber = require('fiber')
local test_utils = require('test.test_utils')
local utils = require('app.tools.utils')
local t2s = require('app.tools.t2s')

local t = require('luatest')
local g = t.group('game')

g.before_each(function()
    test_utils.truncate_all()
end)

g.test_add_suit = function()
    local router = helper.cluster.main_server
    local r = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(r)
    test_utils.suit_status()

    --t.fail()
end

g.test_activate_suit = function()
    local router = helper.cluster.main_server
    local r = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return activate_suit(10111, 10)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return activate_suit(10111, 10)")
    t.assert_not(r)
    test_utils.suit_status()

    --t.fail()
end

g.test_add_initial_suits = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return add_initial_suits()")
    t.assert(r)
    test_utils.suit_status()

    r = test_utils.get_suit_row_count()
    t.assert(r)
    log.info('**** suit_count = '..r)

    --t.fail()
end

g.test_get_card = function()
    local router = helper.cluster.main_server
    local r = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return activate_suit(10111, 10)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return add_suit(10211, 1, 2)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return activate_suit(10211, 20)")
    t.assert(r)
    test_utils.suit_status()

    local storage1 = helper.cluster:server('s1-1')

    r = storage1.net_box:eval("return get_card(1, 1, 1)")
    t.assert(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    r = storage1.net_box:eval("return get_card(1, 1, 1)")
    t.assert(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    r = storage1.net_box:eval("return get_card(1, 1, 1)")
    t.assert(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    local r1, r2
    r1 = storage1.net_box:eval("return box.space.suit:get(10111)")
    t.assert(r1)
    r2 = storage1.net_box:eval("return box.space.suit:get(10211)")
    t.assert(r2)
    t.assert_equals(r1[6] + r2[6], 27)

    --t.fail()
end

g.test_get_card_when_empty = function()
    local router = helper.cluster.main_server
    local r = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return activate_suit(10111, 1)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return add_suit(10211, 1, 2)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return activate_suit(10211, 1)")
    t.assert(r)
    test_utils.suit_status()

    local storage1 = helper.cluster:server('s1-1')

    r = storage1.net_box:eval("return get_card(1, 1, 1)")
    t.assert(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    r = storage1.net_box:eval("return get_card(1, 1, 1)")
    t.assert(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    r = storage1.net_box:eval("return get_card(1, 1, 1)")
    t.assert_not(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    --t.fail()
end

g.test_get_card_with_count = function()
    local router = helper.cluster.main_server
    local r = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return activate_suit(10111, 1)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return add_suit(10211, 1, 2)")
    t.assert(r)
    test_utils.suit_status()

    r = router.net_box:eval("return activate_suit(10211, 1)")
    t.assert(r)
    test_utils.suit_status()

    local storage1 = helper.cluster:server('s1-1')

    r = storage1.net_box:eval("return get_card(1, 1, 1, nil, 1)")
    t.assert(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    r = storage1.net_box:eval("return get_card(1, 1, 1, nil, 2)")
    t.assert_not(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    --t.fail()
end

g.test_get_card_with_exclude_set = function()
    local router = helper.cluster.main_server
    local r = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(r)
    r = router.net_box:eval("return activate_suit(10111, 1)")
    t.assert(r)
    r = router.net_box:eval("return add_suit(10211, 1, 2)")
    t.assert(r)
    r = router.net_box:eval("return activate_suit(10211, 1)")
    t.assert(r)
    r = router.net_box:eval("return add_suit(10311, 1, 2)")
    t.assert(r)
    r = router.net_box:eval("return activate_suit(10311, 1)")
    t.assert(r)
    test_utils.suit_status()

    local storage1 = helper.cluster:server('s1-1')

    r = storage1.net_box:eval("return get_card(1, 1, 1, {10111, 10211})")
    t.assert(r)
    t.assert_equals(r, 10311)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    r = storage1.net_box:eval("return get_card(1, 1, 1, {10111, 10211})")
    t.assert_not(r)
    test_utils.suit_status()
    log.info('**** found_card = '..json.encode(r))

    --t.fail()
end

g.test_game = function()
    local router = helper.cluster.main_server
    local response
    response = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(response)
    response = router.net_box:eval("return activate_suit(10111, 2)")
    t.assert(response)
    response = router.net_box:eval("return add_suit(10211, 1, 2)")
    t.assert(response)
    response = router.net_box:eval("return activate_suit(10211, 2)")
    t.assert(response)
    test_utils.suit_status()

    response = router:http_request('post', '/signup', {json = {}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.user_status()

    local user_uuid = response.json.user_uuid
    response = router:http_request('post', '/usergame', {json = {user_uuid = user_uuid}, raise = false})
    log.info('***** response for /usergame = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.usergame_status()

    --t.fail()
end

g.test_next = function()
    local router = helper.cluster.main_server
    local response
    response = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(response)
    response = router.net_box:eval("return activate_suit(10111, 2)")
    t.assert(response)
    response = router.net_box:eval("return add_suit(10211, 1, 2)")
    t.assert(response)
    response = router.net_box:eval("return activate_suit(10211, 2)")
    t.assert(response)
    response = router.net_box:eval("return add_suit(10311, 1, 2)")
    t.assert(response)
    response = router.net_box:eval("return activate_suit(10311, 2)")
    t.assert(response)
    response = router.net_box:eval("return add_suit(10411, 1, 2)")
    t.assert(response)
    response = router.net_box:eval("return activate_suit(10411, 2)")
    t.assert(response)
    test_utils.suit_status()

    response = router:http_request('post', '/signup', {json = {}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.user_status()

    local user_uuid = response.json.user_uuid
    response = router:http_request('post', '/usergame', {json = {user_uuid = user_uuid}, raise = false})
    log.info('***** response for /usergame = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.usergame_status()

    log.info('**** response = '..json.encode(response))
    fiber.sleep(1)
    local offer_set = response.json.offer_set
    response = router:http_request('post', '/next_level', {json = {user_uuid = user_uuid, move_set = offer_set}, raise = false})
    log.info('***** response for /next_level = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.usergame_status()

    --t.fail()
end

g.test_move_set_1 = function()
    local router = helper.cluster.main_server
    local response
    response = router.net_box:eval("return add_suit(10111, 1, 2)")
    t.assert(response)
    response = router.net_box:eval("return activate_suit(10111, 2)")
    t.assert(response)
    response = router.net_box:eval("return add_suit(10211, 1, 2)")
    t.assert(response)
    response = router.net_box:eval("return activate_suit(10211, 2)")
    t.assert(response)
    test_utils.suit_status()

    response = router:http_request('post', '/signup', {json = {}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.user_status()

    local user_uuid = response.json.user_uuid
    response = router:http_request('post', '/usergame', {json = {user_uuid = user_uuid}, raise = false})
    log.info('***** response for /usergame = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.usergame_status()

    local offer_set = response.json.offer_set
    local wrong_set = {{original_set=offer_set[1].flip_set, flip_set=offer_set[1].original_set}}
    response = router:http_request('post', '/next_level', {json = {user_uuid = user_uuid, move_set = wrong_set}, raise = false})
    log.info('***** response for /next_level = '..json.encode(response))
    t.assert_equals(response.status, 500)
    test_utils.usergame_status()

    --t.fail()
end

g.test_move_set_2 = function()
    test_utils.add_initial_suits()
    
    local r
    local router = helper.cluster.main_server
    r = router:http_request('post', '/signup', {json = {}})
    log.info('***** response for /signup = '..json.encode(r))
    t.assert_equals(r.status, 200)
    t.assert_equals(r.reason, "Ok")
    test_utils.user_status()

    local user_uuid = r.json.user_uuid
    r = router:http_request('post', '/usergame', {json = {user_uuid = user_uuid}, raise = false})
    log.info('***** response for /usergame = '..json.encode(r))
    t.assert_equals(r.status, 200)
    t.assert_equals(r.reason, "Ok")
    test_utils.usergame_status()

    test_utils.update_usergame(user_uuid, 6, 0, 10, 
        {10212,20511,10911,21311,21511},
        {22011,10511,21711,22111},
        {
            {orig_set = {20511}, flip_set = {21711}},
            {orig_set = {10211}, flip_set = {22011}},
            {orig_set = {10911}, flip_set = {23211}},
            {orig_set = {23211}, flip_set = {10511}},
            {orig_set = {21311}, flip_set = {22711}},
            {orig_set = {22711,23211}, flip_set = {22111,10511}},
            {orig_set = {10911}, flip_set = {23111}},
            {orig_set = {21311}, flip_set = {21811}}
        }
    )
    test_utils.usergame_status()
    log.info('---')

    local move_set = {
        {orig_set = {20511}, flip_set = {21711}},
        {orig_set = {10211}, flip_set = {22011}},
        {orig_set = {10911}, flip_set = {23211}},
        {orig_set = {21311}, flip_set = {22711}},
        {orig_set = {22711,23211}, flip_set = {22111,10511}}
    }

    r = router:http_request('post', '/next_level', {json = {user_uuid = user_uuid, move_set = move_set}, raise = false})
    log.info('***** response for /next_level = '..json.encode(r))
    t.assert_equals(r.status, 200)
    test_utils.usergame_status()

    --t.fail()
end

g.test_full = function()
    local storage1 = helper.cluster:server('s1-1')
    local r = storage1.net_box:eval("return add_initial_suits()")
    t.assert(r)
    
    local storage2 = helper.cluster:server('s2-1')
    r = storage2.net_box:eval("return add_initial_suits()")
    t.assert(r)
    
    local router = helper.cluster.main_server
    local response = router:http_request('post', '/signup', {json = {}})
    log.info('***** response = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    test_utils.user_status()
    test_utils.usergame_status()

    
    local user_uuid = response.json.user_uuid
    log.info('**** user_uuid = '..tostring(user_uuid))

    response = router:http_request('post', '/usergame', {json = {user_uuid = user_uuid}, raise = false})
    log.info('***** response for /usergame = '..json.encode(response))
    t.assert_equals(response.status, 200)
    t.assert_equals(response.reason, "Ok")
    
    for level = 1, 30 do        
        log.info('----')
        log.info('**** level = '..tostring(level))
        
        local own_set = response.json.own_set
        local col_set = response.json.col_set
        local offer_set = response.json.offer_set
        log.info('**** own_set = '..json.encode(own_set))
        log.info('**** col_set = '..json.encode(col_set))
        log.info('**** offer_set = '..json.encode(offer_set))

        r = storage1.net_box:eval("return resolve_graph("..
            t2s.t2s(own_set)..", "..
            t2s.t2s(col_set)..", "..
            t2s.t2s(offer_set)..
        ")")
        --[[ local i = 1
        while i < 10 and #r[i] ~= 0 do
            if r[i] ~= nil or #r[i] ~= 0 then
                log.info('**** result '..i..'  = '..t2s.t2s(r[i], 1))
            end
            i = i + 1
        end
        if #r > 9 then log.info('**** result output is stopped') end ]]
        log.info('**** resolve_graph = '..t2s.t2s(r, 2))
        t.assert_not_equals(r, {})
        
        local move_set = {}
        local move_index_set = r[1]
        for _, i in ipairs(move_index_set) do
            table.insert(move_set, offer_set[i])
        end
        log.info('**** move_set = '..json.encode(move_set))        
        
        response = router:http_request('post', '/next_level', {json = {user_uuid = user_uuid, move_set = move_set}, raise = false})
        log.info('***** response for /next_level = '..json.encode(response))
        --test_utils.suit_status()

        t.assert_equals(response.status, 200)
        t.assert_equals(response.reason, "Ok")        
    end
    --t.fail()
end

