local log = require('log')
local json = require('json')
local uuid = require('uuid')
local helper = require('test.helper')

local function user_status()
    local w = {}
    local s = false

    local storage1 = helper.cluster:server('s1-1')
    local storage2
    local f = string.find(json.encode(helper.cluster.replicasets), 's2')
    if f then
        storage2 = helper.cluster:server('s2-1')
        if storage2 and storage2.net_box then
            s = storage2.net_box:is_connected()
        end
    end
    
    log.info("---")
    w = storage1.net_box:eval("return box.space.user:select()")
    log.info("**** storage1, user: "..json.encode(w))
    if s then
        w = storage2.net_box:eval("return box.space.user:select()")
        log.info("**** storage2, user: "..json.encode(w))
    end
    w = storage1.net_box:eval("return box.space.session:select()")
    log.info("**** storage1, session: "..json.encode(w))
    if s then
        w = storage2.net_box:eval("return box.space.session:select()")
        log.info("**** storage2, session: "..json.encode(w))
    end
end

local function usergame_status()
    local w = {}
    local s = false

    local storage1 = helper.cluster:server('s1-1')
    local storage2
    local f = string.find(json.encode(helper.cluster.replicasets), 's2')
    if f then
        storage2 = helper.cluster:server('s2-1')
        if storage2 and storage2.net_box then
            s = storage2.net_box:is_connected()
        end
    end
    
    log.info("---")
    w = storage1.net_box:eval("return box.space.usergame:select()")
    log.info("**** storage1, usergame: "..json.encode(w))
    if s then
        w = storage2.net_box:eval("return box.space.usergame:select()")
        log.info("**** storage2, usergame: "..json.encode(w))
    end
--[[     w = storage1.net_box:eval("return box.space.suit:select()")
    log.info("**** storage1, suit: "..json.encode(w))
    if s then
        w = storage2.net_box:eval("return box.space.suit:select()")
        log.info("**** storage2, suit: "..json.encode(w))
    end ]]
--[[     w = storage1.net_box:eval("return box.space.collection:select()")
    log.info("**** storage1, collection: "..json.encode(w))
    if s then
        w = storage2.net_box:eval("return box.space.collection:select()")
        log.info("**** storage2, collection: "..json.encode(w))
    end
    w = storage1.net_box:eval("return box.space.card:select()")
    log.info("**** storage1, card: "..json.encode(w))
    if s then
        w = storage2.net_box:eval("return box.space.card:select()")
        log.info("**** storage2, card: "..json.encode(w))
    end
    w = storage1.net_box:eval("return box.space.offer:select()")
    log.info("**** storage1, offer: "..json.encode(w))
    if s then
        w = storage2.net_box:eval("return box.space.offer:select()")
        log.info("**** storage2, offer: "..json.encode(w))
    end ]]
end

local function suit_status()
    local w = {}
    local s = false

    local storage1 = helper.cluster:server('s1-1')
    local storage2
    local f = string.find(json.encode(helper.cluster.replicasets), 's2')
    if f then
        storage2 = helper.cluster:server('s2-1')
        if storage2 and storage2.net_box then
            s = storage2.net_box:is_connected()
        end
    end
    
    w = storage1.net_box:eval("return box.space.suit:select()")
    log.info("**** storage1, suit: "..json.encode(w))
    if s then
        w = storage2.net_box:eval("return box.space.suit:select()")
        log.info("**** storage2, suit: "..json.encode(w))
    end
end

local function get_suit_row_count()
    local w = {}
    local s = false

    local storage1 = helper.cluster:server('s1-1')
    return storage1.net_box:eval("return box.space.suit:len()")
end

local function add_initial_suits()
    local w = {}
    local s = false

    local storage1 = helper.cluster:server('s1-1')
    local storage2
    local f = string.find(json.encode(helper.cluster.replicasets), 's2')
    if f then
        storage2 = helper.cluster:server('s2-1')
        if storage2 and storage2.net_box then
            s = storage2.net_box:is_connected()
        end
    end

    w = storage1.net_box:call("add_initial_suits", {})
    if s then
        w = storage2.net_box:call("add_initial_suits", {})
    end    
end

local function set_timeframes(session_lifetime, session_update_timedelta)
    local w = {}
    local s = false

    local storage1 = helper.cluster:server('s1-1')
    local storage2
    local f = string.find(json.encode(helper.cluster.replicasets), 's2')
    if f then
        storage2 = helper.cluster:server('s2-1')
        if storage2 and storage2.net_box then
            s = storage2.net_box:is_connected()
        end
    end

    w = storage1.net_box:call(
        "set_timeframes", 
        {session_lifetime, session_update_timedelta}
    )
    if s then
        w = storage2.net_box:call(
            "set_timeframes", 
            {session_lifetime, session_update_timedelta}
        )
    end
end

local function update_usergame(user_uuid, moves, balance, level, own_set, col_set, offer_set)
    local w = {}
    local s

    local storage = helper.cluster:server('s1-1')
    w = storage.net_box:call("box.space.user:get", {uuid.fromstr(user_uuid)})
    if w == nil then
        local f = string.find(json.encode(helper.cluster.replicasets), 's2')
        if f then
            storage = helper.cluster:server('s2-1')
            if storage and storage.net_box then
                s = storage.net_box:is_connected()
                if not s then
                    return false
                end
            end
        end
    end

    --w = storage.net_box:call("box.begin", {})
    w = storage.net_box:call(
        "box.space.usergame:update",
        {
            uuid.fromstr(user_uuid),
            {
                {'=', 'moves', moves}, 
                {'=', 'balance', balance},
                {'=', 'level', level},
                {'=', 'col_set', col_set},
                {'=', 'own_set', own_set},
                {'=', 'offer_set', offer_set}
            }
        }
    )
    --w = storage.net_box:call("box.commit", {})
end

local function truncate_all()
    helper.truncate_space_on_cluster(helper.cluster, 'user')
    helper.truncate_space_on_cluster(helper.cluster, 'session')
    helper.truncate_space_on_cluster(helper.cluster, 'usergame')
    helper.truncate_space_on_cluster(helper.cluster, 'suit')
end

return {
    user_status = user_status,
    usergame_status = usergame_status,
    suit_status = suit_status,
    get_suit_row_count = get_suit_row_count,
    add_initial_suits = add_initial_suits,
    set_timeframes = set_timeframes,
    update_usergame = update_usergame,
    truncate_all = truncate_all
}