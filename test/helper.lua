local fio = require('fio')
local log = require('log')
local t = require('luatest')
local fiber = require('fiber')
local cartridge_helpers = require('cartridge.test-helpers')

local helper = {}

helper.root = fio.dirname(fio.abspath(package.search('init')))
helper.datadir = fio.pathjoin(helper.root, 'tmp', 'db_test')
helper.server_command = fio.pathjoin(helper.root, 'init.lua')

helper.cluster = cartridge_helpers.Cluster:new({
    server_command = helper.server_command,
    datadir = helper.datadir,
    use_vshard = true,
    replicasets = {
        {
            alias = 'router',
            uuid = cartridge_helpers.uuid('a'),
            roles = {'router'},
            servers = {
                { instance_uuid = cartridge_helpers.uuid('a', 1), alias = 'r1'},
            },
        },
        {
            alias = 's1',
            uuid = cartridge_helpers.uuid('b'),
            roles = {'storage'},
            servers = {
                { instance_uuid = cartridge_helpers.uuid('b', 1)}, 
                { instance_uuid = cartridge_helpers.uuid('b', 2)},
            },
        },
        {
            alias = 's2',
            uuid = cartridge_helpers.uuid('c'),
            roles = {'storage'},
            servers = {
                { instance_uuid = cartridge_helpers.uuid('c', 1)}, 
                { instance_uuid = cartridge_helpers.uuid('c', 2)},
            },
        },
    }
})

function helper.truncate_space_on_cluster(cluster, space_name)
    assert(cluster ~= nil)
    for _, server in ipairs(cluster.servers) do
        server.net_box:eval([[
            local space_name = ...
            local space = box.space[space_name]
            if space ~= nil and not box.cfg.read_only then
                space:truncate()
            end
        ]], {space_name})
    end
end

t.before_suite(function()
    fio.rmtree(helper.datadir)
    fio.mktree(helper.datadir)
    helper.cluster:start()
end)

t.after_suite(function()
    --fiber.sleep(300)
    helper.cluster:stop()
end)

return helper